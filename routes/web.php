<?php

use Illuminate\Support\Facades\Route;


//This is for big commerce setup routes
Route::group(['namespace' => 'Bigcommerce','prefix' => 'auth'], function () { 	
	Route::get('/install', 'AppSetup@appInstall');
	Route::get('/uninstall', 'AppSetup@appUnInstall');
	Route::get('/load', 'AppSetup@appLoad'); 
	Route::get('/removeUser', 'AppSetup@removeUser');
	Route::get('/error', 'AppSetup@error');
	Route::get('/webhooks/{id}', 'AppSetup@webhooks');
});


Route::get('/faq', 'SPAController@faq');
Route::get('/privacy-policy', 'SPAController@privacyPolicy');
Route::get('/terms-of-services', 'SPAController@termsOfService');

Route::get('{path}', 'SPAController@index')->where('path', '(.*)'); 
 