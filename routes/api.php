<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

 
//Webhooks
Route::group(['middleware' => 'api','prefix' => '/','namespace' => 'Bigcommerce'], function(){ 
    Route::post('bc_webhook', 'BcWebHookController@bcWebhooks'); //big commerce webhooks
    Route::post('stripe_webhook', 'StripeWebHookController@stripeWebHook'); //stripe webhooks
});   

/*
* Routes for without login
*/
Route::group(['middleware' => 'api','prefix' => '/','namespace' => 'Api'], function(){
   Route::post('login', 'AuthController@login'); 
    
});


/*
* Routes for with login but not plan check
*/
Route::group(['middleware' => ['auth:api'],'namespace' => 'Api'], function(){

   //Logout App
   Route::post('logout', 'AuthController@logout'); 

    //App Status
   Route::get('getAppStatus', 'StoreAppStatusController@index');
   Route::post('saveAppStatus', 'StoreAppStatusController@save');
   Route::post('applyAppChanges', 'StoreAppStatusController@applyAppChanges'); 

   //Contact Support
   Route::post('contactSupport', 'CommonController@contactSupport'); 

   //Stripe Plans
   Route::get('getSubscriptionPlans', 'StripeController@subscriptionPlans');
   Route::get('getSubscriptionPlan/{id}', 'StripeController@subscriptionPlan');
   Route::post('subscriptionCreate', 'StripeController@subscriptionCreate');
   Route::get('getPlanStatus/{id}', 'StripeController@getPlanStatus'); 


});

/*
* Routes for with login
*/
Route::group(['middleware' => ['auth:api','verifyplan'],'namespace' => 'Api'], function(){

   //Logout App
   Route::post('logout', 'AuthController@logout'); 

   //Dashboard Data
   Route::get('dashboard', 'DashboardController@getDashboard');

   //Store Profile
   Route::get('profile', 'CommonController@getStoreProfile');


});