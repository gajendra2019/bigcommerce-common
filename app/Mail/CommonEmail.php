<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommonEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromEmail = isset($this->data['from_email'])?$this->data['from_email']:(env('MAIL_FROM_ADDRESS'));
        $fromName = isset($this->data['from_name'])?$this->data['from_name']:(env('MAIL_FROM_NAME'));
        $emailSubject = isset($this->data['subject'])?$this->data['subject']:'No Subject';
        return $this->view('email.'.$this->data['view'])
            ->from($fromEmail,$fromName)
            ->subject($emailSubject) 
            ->with(['data' => $this->data]);
    }
}