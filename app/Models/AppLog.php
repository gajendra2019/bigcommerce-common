<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AppLog extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = "app_log";

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'store_id', 
        'store_name', 
        'store_email', 
        'address',
        'country',
        'country_code', 
        'phone',  
        'deleted_at',
        'status'
    ]; 


}


