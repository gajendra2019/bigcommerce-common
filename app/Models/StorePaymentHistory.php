<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StorePaymentHistory extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = "store_payment_history";

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'store_id', 
        'sub_id', 
        'customer', 
        'created',
        'current_period_start',
        'current_period_end', 
        'collection_method',  
        'quantity',
        'plan_id',
        'plan_active',
        'plan_amount',
        'plan_currency',
        'plan_interval',
        'plan_interval_count',
        'plan_nickname',
        'plan_product',
    ]; 


}


