<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = "store_plans";

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'plan_id', 
        'product_id', 
        'plan_name', 
        'plan_duration',
        'plan_price',
        'plan_type', 
        'plan_currency',  
        'plan_description',
        'status'
    ]; 


}


