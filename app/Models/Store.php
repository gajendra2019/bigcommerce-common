<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model; 

use App\Models\Plans;

class Store extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = "stores";

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'store_id',
        'store_name',
        'store_email',
        'store_hash_key',
        'access_token', 
        'address',
        'country',
        'country_code', 
        'phone', 
        'store_url',
        'control_panel_base_url',
        'store_logo_url',
        'order_email',
        'currency',
        'currency_symbol',
        'bc_jsfile',
        'bc_uuid',
        'stripe_client_id',
        'stripe_sub_id',
        'stripe_plan_id',
        'start_date',
        'end_date',
        'cancel_date',
        'deleted_at',
        'status'
    ];  


    public function planInfo() {
        return $this->hasOne(Plans::class, 'plan_id','stripe_plan_id');
    }


     /**
    * Get table information by shop id
    *
    * #Retrieving A Single Row / Column From A Table
	*
	* @param tableName (string), WhereCondition (array)
	* @return array
    */

    public function getStoreTableInformationByID( $tableName, $whereCondition ){        
        $results = DB::table($tableName)->where($whereCondition)->first();
        if( $results ) {
           return $results;
        }
        return '';
    }

     /**
	* Common update Or Insert information into database
	*
	* @param $insertData ( array ), TableName(string), $whereCondition(array)
	* @return null
    */

    public function commonInsertOrUpdateTableformation( $TableName , $whereCondition, $insertData ){
        DB::table($TableName)->updateOrInsert(
            $whereCondition,
            $insertData            
        );
    }


      /**
	* add Store information
	*
	* @param $insertData ( array )
	* @return null
    */

    public function addStoreInformation( $insertData ){
        DB::table('stores')->insert(
            $insertData
        );
    }

    /**
	* Update Store information
	*
	* @param $insertData ( array ), $whereCondition ( array )
	* @return null
    */

    public function updateStoreInformation( $insertData, $whereCondition ){

        DB::table('stores')->updateOrInsert(
            $whereCondition,
            $insertData
        );
    }




    /**
    * Description:  This function tell us. check Access token
    *
    * @param token 
    * @return array/false
    */

    public function isGetStoreInformationAuthTokenValid( $token ) {        
        $shopDetail = $this->getStoreTableInformationByID('stores', array( "access_token" => $token ) );        
        $data = array();
        if( $shopDetail ) {
            $data['StoreName'] = $shopDetail->store_name;
            $data['storeID'] = $shopDetail->store_id;
            $data['storeAccessToken'] = $shopDetail->access_token;
            $data['storeAuthToken'] = $shopDetail->store_hash_key;
            $data['storeEnabled'] = $shopDetail->status;
            return $data;
        }
        return false;
    }

}


