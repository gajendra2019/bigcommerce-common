<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreAppStatus extends Model
{
    use HasFactory;
    
    protected $table = "store_app_status"; 
    protected $fillable = [
        'store_id',
        'is_show',
        'is_setting_updated'
    ];  
}
