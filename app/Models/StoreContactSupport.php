<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreContactSupport extends Model
{
    use HasFactory;

    protected $table = "store_contact_support"; 
    protected $fillable = [
        'store_id',
        'sender_name',
        'sender_email',
        'sender_subject',
        'message',
        'receiver_email'
    ];  



}
