<?php 
/**
 * @author [gajendra]
 * @email [gajendra@bitcot.com]
 * @create date 2021-01-09 13:09:34
 * @modify date 2021-01-09 13:09:34
 * @desc [ common email function collections ]
 */
namespace App\Utils;    
//models
use App\Models\Store;  
use App\Models\User;  
use App\Models\StoreContactSupport;

//email  
use Illuminate\Support\Facades\Mail;
use App\Mail\CommonEmail; 

class EmailHelper {   

    //Function for send email on app install Client
    public static function appInstall($store_id){
        $store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$store) {
            return response()->json([
                'status' => true,
                'message' => "Invalid store id :$store_id"
            ]);
        }   
        $data = array(
            'view' => 'install_user',
            'subject' => 'Thank you for Installing our '.env('APP_NAME').' App',
            'store' => $store,'receiver' => [
                'name' => $store->store_name,
                'email' => $store->store_email
            ]
        );  
        Mail::to($store->store_email)->send(new CommonEmail($data)); 
        self::common_admin($store,'App Install');
    }

    //Function for send email on app UnInstall Client
    public static function appUninstall($store_id){
        $store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$store) {
            return response()->json([
                'status' => true,
                'message' => "Invalid store id :$store_id"
            ]);
        }   
        $data = array(
            'view' => 'uninstall_user',
            'subject' => 'We just want to know your valuable feedback',
            'store' => $store,
            'receiver' => [
                'name' => $store->store_name,
                'email' => $store->store_email
            ]
        );  
        Mail::to($store->store_email)->send(new CommonEmail($data));       
        self::common_admin($store,'App Uninstall');  
    }

    //Function for send email on Contact Support Client
    public static function contactSupportClient($store_id,$enqId = 0){
        $store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$store) {
            return response()->json([
                'status' => true,
                'message' => "Invalid store id :$store_id"
            ]);
        }    
        $enqData = StoreContactSupport::where(['store_id' => $store_id,'id' => $enqId])->get()->first();  
        $data = array(
            'view' => 'contact_support_client',
            'subject' => isset($enqData->sender_subject)?$enqData->sender_subject:'No Subject', 
            'receiver' => [
                'name' => $store->store_name,
                'email' => $store->store_email
            ],
            'store' => $store,
            'enqData' => [
                'name' => $store->store_name,
                'email' => $store->store_email,
                'subject' => isset($enqData->sender_subject)?$enqData->sender_subject:'No Subject',
                'message' => isset($enqData->message)?$enqData->message:'No Message',
            ]
        );  
        Mail::to($store->store_email)->send(new CommonEmail($data));  
    }

    //Function for send email on Contact Support Admin
    public static function contactSupportAdmin($store_id,$enqId = 0){       
        $store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$store) {
            return response()->json([
                'status' => true,
                'message' => "Invalid store id :$store_id"
            ]);
        }    
        $enqData = StoreContactSupport::where(['store_id' => $store_id,'id' => $enqId])->get()->first();  
        $data = array(
            'view' => 'contact_support_admin',
            'subject' => isset($enqData->sender_subject)?$enqData->sender_subject:'No Subject', 
            'receiver' => [
                'name' => env('APP_NAME').' admin',
                'email' => env('MAIL_FROM_ADDRESS')
            ],
            'from_email' => $store->store_email,
            'from_name' => $store->store_name,
            'store' => $store,
            'enqData' => [
                'name' => $store->store_name,
                'email' => $store->store_email,
                'subject' => isset($enqData->sender_subject)?$enqData->sender_subject:'No Subject',
                'message' => isset($enqData->message)?$enqData->message:'No Message',
            ]
        );  
        Mail::to($store->store_email)->send(new CommonEmail($data));               
    }

    //Function for send email on plan activation Client
    public static function planActivation($store_id){ 
        $store = Store::where(['store_id' => $store_id,'status' => '1'])->with('planInfo')->get()->first(); 
        if(!$store) {
            return response()->json([
                'status' => true,
                'message' => "Invalid store id :$store_id"
            ]);
        }   
        $data = array(
            'view' => 'subscription_activation_user',
            'subject' => 'Your Recurring charge has been activated',
            'store' => $store,
            'receiver' => [
                'name' => $store->store_name,
                'email' => $store->store_email
            ]
        );  
         Mail::to($store->store_email)->send(new CommonEmail($data));  
         self::common_admin($store,'Subscription Activation');  
    }

    //Function for send email on plan cancel Client
    public static function planCancelled($store_id){
        $store = Store::where(['store_id' => $store_id,'status' => '1'])->with('planInfo')->get()->first(); 
        if(!$store) {
            return response()->json([
                'status' => true,
                'message' => "Invalid store id :$store_id"
            ]);
        }   
        $data = array(
            'view' => 'subscription_cancel_user',
            'subject' => 'Your Recurring charge has been expired',
            'store' => $store,
            'receiver' => [
                'name' => $store->store_name,
                'email' => $store->store_email
            ]
        );  
         Mail::to($store->store_email)->send(new CommonEmail($data));  
         self::common_admin($store,'Subscription Cancelled');        
    } 
    
    //common email to admin
    public static function common_admin($store,$subject='') {
        $data = array(
            'view' => 'common_admin',
            'subject' => $subject,
            'store' => $store,
            'receiver' => [
                'name' => 'Support',
                'email' => env('SUPPORT_EMAIL')
            ]
        );  
        Mail::to(env('SUPPORT_EMAIL'))->send(new CommonEmail($data));
    }
    

    //Add more to enhanced
} 
?>