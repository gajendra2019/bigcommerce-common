<?php
/**
 * @author [gajendra]
 * @email [gajendra@bitcot.com]
 * @create date 2021-01-09 13:09:34
 * @modify date 2021-01-09 13:09:34
 * @desc [ common stripe function collections ]
 */
namespace App\Utils; 
use Stripe;
use Stripe\StripeClient;

//Models
use App\Models\Store;
use App\Models\Plans; 
use App\Models\StorePaymentHistory;

//Utils Libraries
use App\Utils\EmailHelper;

class StripeHelper {   
    
    //Get Stripe Key
    public static function stripe_sk(){ 
        $env = env('APP_ENV'); 
        switch ($env) {
            case 'local':
            case 'development':   
            case 'testing':     
                return config('constants.stripe_dev.STRIPE_SK');
                break;
            case 'production':     
                return config('constants.stripe.STRIPE_SK');
                break; 
            default:
                #TODO MORE
                break;
        } 
    }
    
    //Function for get existing stripe customer detail
    public static function getOneCustomer($customer_id){
        $stripe = new \Stripe\StripeClient( self::stripe_sk() );
        try {
            $allCustomers = $stripe->customers->retrieve($customer_id);
            return [
                'status' => true,
                'error' => null,
                'data' => $allCustomers
            ];
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }catch (\Stripe\Exception\AuthenticationException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } catch(Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }
    }
    
    //Function for create new stripe customer
    public static function createOneCustomer($data,$token){
        $stripe = new \Stripe\StripeClient( self::stripe_sk() );
        try {
            $created = $stripe->customers->create($data); 
            return [
                'status' => true,
                'error' => null,
                'data' => $created
            ];
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }catch (\Stripe\Exception\AuthenticationException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } catch(Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } 
    }

    //Function for create new subscription
    public static function createSubscription($customer_id,$pirce_plan_id){
        $stripe = new \Stripe\StripeClient( self::stripe_sk() );
        try {
            $created = $stripe->subscriptions->create([
                'customer' => $customer_id,
                'items' => [
                   ['price' => $pirce_plan_id],
                ],
              ]); 
            return [
                'status' => true,
                'error' => null,
                'data' => $created
            ];
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }catch (\Stripe\Exception\AuthenticationException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } catch(Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } 
    } 

    //Function for retrive subscription detail
    public static function retrieveSubscription($sub_id){
        $stripe = new \Stripe\StripeClient( self::stripe_sk() );
        try {
            $retrieve = $stripe->subscriptions->retrieve($sub_id); 
            return [
                'status' => true,
                'error' => null,
                'data' => $retrieve
            ];
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }catch (\Stripe\Exception\AuthenticationException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } catch(Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } 
    } 

    //Function for cancel stripe subscription
    public static function cancelSubscription($sub_id){
        $stripe = new \Stripe\StripeClient( self::stripe_sk() );
        try {
            $cancelled = $stripe->subscriptions->cancel($sub_id); 
            return [
                'status' => true,
                'error' => null,
                'data' => $cancelled
            ];
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }catch (\Stripe\Exception\AuthenticationException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } catch(Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } 
    } 

    //Function for update stripe subscription
    public static function updateSubscription($sub_id, $data = array()){
        $stripe = new \Stripe\StripeClient( self::stripe_sk() );
        try {
            $updated = $stripe->subscriptions->update($sub_id,['meta' => $data]); 
            return [
                'status' => true,
                'error' => null,
                'data' => $updated
            ];
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }catch (\Stripe\Exception\AuthenticationException $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } catch(Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        } 
    } 

    //BELOW ALL FUNCTIONS IS FOR DB    
    //Function for create customer for DB USE
    public static function customer($store_id,$stripe_token) { 
        $storeModel = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$storeModel) {
            return array(
                'status' => false,
                'error' => 'Invalid store ID'
            );
        } 
        $clientObject = array(
            'name' => $storeModel->store_name,
            'email' => $storeModel->store_email,
            'phone' => $storeModel->phone,
            "source" => $stripe_token,
            'description' => 'Mywishlist BigCommerece Store Client Plan Taken'
        );
        $stripe = new \Stripe\StripeClient(self::stripe_sk());
        if( ($storeModel->stripe_client_id != null) || ( $storeModel->stripe_client_id != '') ){ 
            $check = self::getOneCustomer($storeModel->stripe_client_id);
            if( ($check['status'] == true)) {
                return array(
                    'status' => true,
                    'message' => 'Customer setup successfully',
                    'customer_id' => $storeModel->stripe_client_id 
                );
            } else {
                $result = self::createOneCustomer($clientObject,$stripe_token);
                if($result['status'] == true) {
                    $storeUpdated = $storeModel->update(['stripe_client_id' => $result['data']['id']]);
                    return array(
                        'status' => true,
                        'message' => 'Customer setup successfully',
                        'customer_id' => $result['data']['id'] 
                    ); 
                } 
                return $result;                
            }            
        }else{
            $result = self::createOneCustomer($clientObject,$stripe_token); 
            if($result['status'] == true) {
                $storeUpdated = $storeModel->update(['stripe_client_id' => $result['data']['id']]);
                return array(
                    'status' => true,
                    'message' => 'Customer setup successfully',
                    'customer_id' => $result['data']['id']
                );  
            }
            return $result;            
        }
    } 

    //create subscription for scpecific customer
    public static function plan($customer_id,$plan_id,$store_id) { 
        $storeModel = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$storeModel){
            return array(
                'status' => false,
                'error' => 'No store found'
            );
        }
        $planModel = Plans::where(['plan_id' => $plan_id,'status' => '1'])->get()->first();
        if(!$planModel){
            return array(
                'status' => false,
                'error' => 'No plan found'
            );
        } 

         
        $result = self::createSubscription($customer_id,$plan_id);
        if($result['status'] == false){
            return $result;
        } 

        $startDate = date('Y-m-d H:i:s',($result['data']['current_period_start'])); 
        $endDate = date('Y-m-d H:i:s', ($result['data']['current_period_end']));  

        $storeModel->update([
            'stripe_sub_id' => $result['data']['id'],
            'stripe_plan_id' => $plan_id,
            'start_date' => $startDate, 
            'end_date' => $endDate
        ]);
        return array(
            'status' => true,
            'error' => null,
            'message' => 'Subscription created successfully'
        );          
    }  

    //function for save payment history
    public static function save_payment_history($sub_id,$store_id){
        $storeModel = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$storeModel) {
            return array(
                'status' => false,
                'error' => 'Invalid store ID'
            );
        } 
        $subsDetail = self::retrieveSubscription($sub_id);
        if( isset($subsDetail['status']) && ($subsDetail['status'] == true) ) {
            $sdata = $subsDetail['data'];
            $payemntObject = new StorePaymentHistory();
            $payemntObject->create([
                'store_id' => $store_id, 
                'sub_id' => $sdata->id, 
                'customer' => $sdata->customer, 
                'created' => $sdata->created,
                'current_period_start' => $sdata->current_period_start,
                'current_period_end' => $sdata->current_period_end, 
                'collection_method' => $sdata->collection_method,  
                'quantity' => $sdata->quantity,
                'plan_id' => $sdata->plan->id,
                'plan_active' => $sdata->plan->active,
                'plan_amount' => $sdata->plan->amount,
                'plan_currency' => $sdata->plan->currency,
                'plan_interval' => $sdata->plan->interval,
                'plan_interval_count' => $sdata->plan->interval_count,
                'plan_nickname' => $sdata->plan->nickname,
                'plan_product' => $sdata->plan->product
            ]);
            return true;
        } 

    } 

    //cancel subscription for scpecific customer/store
    public static function plan_cancel($store_id) { 
        $storeModel = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
        if(!$storeModel){
            return array(
                'status' => false,
                'error' => 'No store found'
            );
        }  
        //sub_ITJOccvZC8Id9Y
        if($storeModel->stripe_sub_id!='') { 
            $result = self::cancelSubscription($storeModel->stripe_sub_id);
            if($result['status'] == true) {
                $storeModel->update([ 
                    'end_date' => date('Y-m-d h:i:s'),
                    'cancel_date' => date('Y-m-d h:i:s'),
                ]);
                EmailHelper::planCancelled($store_id);//send email for plan cancelled
            } 
            return $result; 
        } 
    } 
 
}