<?php 
/**
 * @author [gajendra]
 * @email [gajendra@bitcot.com]
 * @create date 2021-01-09 13:09:34
 * @modify date 2021-01-09 13:09:34
 * @desc [ common bigcommerce Api function collections ]
 */

namespace App\Utils;   

use Illuminate\Http\Request; 
use Illuminate\Http\Response;
 
//GuzzleHttp
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client; 

//models
use App\Models\Store; 
use App\Models\AppLog;
use App\Models\User; 

class BcApiHelper {   
	 
	// Get All Orders
    public static function getAllOrders($store_id,$last12 = true,$limit=250,$page=1) {         
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
        }  
        
        $extraFilter = '';        
        if($last12 == true) {
            $today = date('Y-m-d');
            $back12Month = date('Y-m-d',strtotime("-12 month"));
            $extraFilter = "max_date_created=$today&min_date_created=$back12Month&";
        }          
        $extraFilter = "limit=$limit&page=$page";  
        
        
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token']; 
    	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v2/orders?$extraFilter",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "content-type: application/json",
		    "x-auth-token: $authToken"
		  ),
		));
		$curlResponse = curl_exec($curl);
		curl_close($curl);  
        $response = json_decode($curlResponse,true);           
        if( isset($response['status']) && ($response['status'] != '200') ){
            return ['status' => false,'message' => 'Not Getting any product','response' => $response];  
		} else {
            return ['status' => true,'response' => $response]; 
        } 
    } 

    // Get One Orders
    public static function getOneOrders($store_id, $order_id) {    
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}   
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token']; 
    	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v2/orders/$order_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "content-type: application/json",
		    "x-auth-token: $authToken"
		  ),
		));
		$curlResponse = curl_exec($curl);
		curl_close($curl);  
		$response = json_decode($curlResponse,true);  
        if(isset($response['id'])){
            return ['status' => true,'response' => $response];  
		} else {
            return ['status' => false,'response' => $response]; 
        } 
	}
	
	// Get One Orders
    public static function getOrderLines($store_id, $order_id) {    
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}   
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token']; 
    	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v2/orders/$order_id/products",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "content-type: application/json",
		    "x-auth-token: $authToken"
		  ),
		));
		$curlResponse = curl_exec($curl);
		curl_close($curl);  
        $response = json_decode($curlResponse,true); 
        if( isset($response['status']) && ($response['status'] != '200') ){
            return ['status' => false,'message' => 'Not Getting any product','response' => $response];  
		} else {
            return ['status' => true,'response' => $response]; 
        } 
    }

    // Get Single Product
    public static function getProductDetail($store_id,$product_id){    
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}  
    	$clientId = env('BC_APP_CLIENT_ID');
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token']; 
    	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v3/catalog/products/".$product_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "content-type: application/json",
		    "x-auth-token: $authToken"
		  ),
		));
		$curlResponse = curl_exec($curl);
		curl_close($curl);  
        $response = json_decode($curlResponse,true); 
        if( isset($response['status']) && ($response['status'] != '200') ){
            return ['status' => false,'message' => 'Not Getting any product','response' => $response];  
		} else {
            return ['status' => true,'response' => $response['data']]; 
        } 
    } 

    // Get all Products
    public static function getAllProducts($store_id,$limit = 250,$page = 1){    
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}  
    	 
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token'];  
    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v3/catalog/products?limit=$limit&page=$page",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "x-auth-token: $authToken"
		  ),
		)); 
		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
		  return ['error' => $err];
		} else {
			$response = json_decode($response,true); 
		    return $response;
		} 
        
    } 

    // Get Product Image
    public static function getProductImage($store_id,$product_id){  
		$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first();  
		//echo '<pre>';print_r($store);die;
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}  
    	$clientId = env('BC_APP_CLIENT_ID');
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token']; 
    	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v3/catalog/products/".$product_id."/images",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "store_hash: $storeHashKey",
		    "X-Auth-Client: $clientId",
		    "X-Auth-Token: $authToken"
		  ),
		));
		$curlResponse = curl_exec($curl);
		curl_close($curl);  
		$response = json_decode($curlResponse,true);   
        if( isset($response['status']) && ($response['status'] != '200') ){
            return ['status' => false,'message' => 'Not Getting any product','response' => $response];  
		} else { 
			$imageData = null; 
			if( isset($response['data']) ){  
				if( count($response['data']) > 1 ) {
					foreach($response['data'] as $k => $image){
						if( (isset($image['is_thumbnail'])) && ($image['is_thumbnail'] == true) ){
							$imageData  = $image; 
						} 
					} 
				}
			} 
            return array(
				'status' => true,
				'response' => $imageData
			);
            
        } 
	} 
	
	// Get Single Customer
	public static function getCustomerDetail($store_id,$customer_id){  
		$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first();  
		//echo '<pre>';print_r($store);die;
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}  
    	$clientId = env('BC_APP_CLIENT_ID');
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token']; 
    	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v3/customers?id%3Ain=".$customer_id."&limit=1",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array( 
		    "X-Auth-Token: $authToken"
		  ),
		));
		$curlResponse = curl_exec($curl);
		curl_close($curl);  
        $response = json_decode($curlResponse,true);  
        if( isset($response['status']) && ($response['status'] != '200') ){
            return ['status' => false,'message' => 'Not Getting any product','response' => $response];  
		} else { 
			$imageData = null; 
			if( isset($response['data']) ){ $imageData = $response['data'][0]; } 
            return array(
				'status' => true,
				'response' => $imageData
			);
            
        } 
    }  

    // Get All customers
    public static function getAllCustomers($store_id){    
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}  
    	
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token']; 

    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v2/customers",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "content-type: application/json",
		    "x-auth-token: $authToken"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
		  return [];
		} else {
			$response = json_decode($response,true);
			return $response; 
		}
        
    }    

    // Get One customers
    public static function getOneCustomers($store_id ,$customer_id){    
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}   
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token'];  
    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v2/customers/$customer_id",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $authToken"
			  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
		  return array('status' => false ,'error' => $err);
		} else {
		  $response  = json_decode($response,true);
		  return array('status' => true ,'data' => $response);
		} 
    } 

    // Get All Store Detail
    public static function getStoreDetail($storeHashKey, $authToken){  
    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v2/store",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $authToken"
			  ),
		)); 
		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
		  return ['status' => false,'error' => $err];
		} else {
		  $response = json_decode($response,true);
		  return ['status' => true ,'data' => $response];
		}  
    }   

    //Get All Product Variants
    public static function getAllProductVariants($store_id,$product_id,$limit=250,$page=1){   
    	$store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}   
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token'];    
    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v3/catalog/products/$product_id/variants?limit=$limit&page=$page",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "content-type: application/json",
		    "x-auth-token: $authToken"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl);  
		if ($err) {
		  return ['status' => false,'error' => $err];
		} else {
		  $response = json_decode($response,true);
		  return ['status' => true ,'data' => $response];
		}  
    }   

    //Get Product One Variant
    public static function getOneProductVariant($store_id,$product_id,$var_id){   
    	$store = Store::where(['store_id' =>$store_id,'status' => '1'])->get()->first(); 
    	if(!$store) {   
    		echo json_encode([
	    			'status' => 'false',
	    			'message' => 'Invalid Store ID',
	    			'data' => []
	    		]);die; 
    	}   
    	$storeHashKey = $store['store_hash_key'];
    	$authToken =  $store['access_token'];    
    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v3/catalog/products/$product_id/variants/$var_id?include_fields=id%2Cproduct_id%2Coption_values%2Csku",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $authToken"
			  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return ['status' => false,'error' => $err];
		} else {
		  $response = json_decode($response,true);
		  return ['status' => true ,'data' => $response];
		}  
    }   	 
 
}