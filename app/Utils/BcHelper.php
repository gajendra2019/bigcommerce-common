<?php 

namespace App\Utils;   

use Illuminate\Http\Request; 
use Illuminate\Http\Response;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB; 
use Validator;

//Models
use Spatie\Permission\Models\Role; 

//GuzzleHttp
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client; 

//models
use App\Models\Store; 
use App\Models\AppLog;
use App\Models\User;  
use App\Models\StorePaymentHistory; 

class BcHelper {   

	public static function getAssetURL() { 
        $assetURL = env('ASSET_URL');
        if(substr($assetURL , -1) != '/' ){
            $assetURL.='/';
		} 
		return $assetURL;
    }	

    public static function getAppURL() { 
        $appURL = env('APP_URL');
        if(substr($appURL , -1) != '/' ){
            $appURL.='/';
		} 
		return $appURL;
    }		
	
	// Get Store Detail
    public static function getStoreDetail($storeHashKey, $authToken){  
    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v2/store",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $authToken"
			  ),
		)); 
		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
		  return ['status' => false,'error' => $err];
		} else {
		  $response = json_decode($response,true);
		  return ['status' => true ,'data' => $response];
		}  
	}   
	
	//save store detail & create login user
	public static function createStoreAndClient($data) {  
		$storeHashKey = explode("/",$data['context'])[1];
		$accessToken = $data['access_token']; 
		$storeDetail = self::getStoreDetail($storeHashKey,$accessToken); 
		if($storeDetail['status'] == true){
			$storeModel = Store::where(['store_id' => $storeHashKey])->get()->first();
            if($storeModel){
            	$storeModel->delete();
			}  
			$store = new Store();
			$storeD = $storeDetail['data'];

			$startDate = date('Y-m-d h:i:s');
            $endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + 3 days')); 

			$storeCreated = $store->create([
				'store_id' => $storeHashKey,
				'store_name' => $storeD['name'],
				'address' => $storeD['address'],
				'country' => $storeD['country'],
				'country_code' => $storeD['country_code'],
				'phone' => $storeD['phone'],
				'store_email' => $storeD['admin_email'],
				'store_hash_key' => $storeHashKey,
				'access_token' => $accessToken,
				'store_url' => $storeD['secure_url'],
				'control_panel_base_url' => $storeD['control_panel_base_url'],
				'store_logo_url' => isset($storeD['logo']['url'])?$storeD['logo']['url']:null,
				'order_email' => $storeD['order_email'],
				'currency' => $storeD['currency'],
				'currency_symbol' => $storeD['currency_symbol'],
				'start_date' => $startDate,
                'end_date' => $endDate,
                'status' => '1'
			]);
			if($storeCreated) { 
				$user = User::where(['email' => $storeD['admin_email']])->get()->first();
                if(!$user) {
                    $model = new User(); 
                    $un = explode(' ', $store['name']);
                    $user = $model->create([
                        'name' => isset($storeD['name'])?$storeD['name']:'', 
                        'email' => $storeD['admin_email'],
                        'password' => Hash::make($storeHashKey),
                        'store_uid' => $storeCreated->id,
                        'store_id' => $storeHashKey,
                        'status' => '1'  
					]);   					
                   // $userModel->assignRole('admin');  
                } else { 
                     $user->update([
                        'password' => Hash::make($storeHashKey),
                        'store_uid' => $storeCreated->id,
                        'store_id' => $storeHashKey, 
					]);  
				}    
				self::appInstallLog($storeHashKey);	 //create app log				
				self::updateScriptJs($storeHashKey); //create JS FILE
				return ['status' => true,'message' => 'store & user setup successfully','user' => $user];

			} else {
				return ['status' => false,'message' => 'Error in create store'];
			} 
		} else {
			return $storeDetail;
		} 

	} 

	 

	//callback function for create app log on install
	public static function appInstallLog($store_id) {
		$storeDetial = Store::select('*')->where(["store_id" => $store_id,"status" => '1'])->first();  
        if(!$storeDetial) {
            return false;
		} 	
		$appLog = new AppLog();
		$appLog->create([
			'store_id' => $storeDetial->store_id, 
			'store_name' => $storeDetial->store_name, 
			'store_email' => $storeDetial->store_email, 
			'address' => $storeDetial->address,
			'country' => $storeDetial->country,
			'country_code' => $storeDetial->country_code, 
			'phone' => $storeDetial->phone, 		 
			'deleted_at' => date('Y-m-d h:i:s'),
			'status' => '1'
		]);
	}
	
	//callback function for create app log on uninstall
	public static function appUnInstallLog($store_id) {
		$storeDetial = Store::select('*')->where(["store_id" => $store_id,"status" => '1'])->first();  
        if(!$storeDetial) {
            return false;
		} 	
		$appLog = new AppLog();
		$appLog->create([
			'store_id' => $storeDetial->store_id, 
			'store_name' => $storeDetial->store_name, 
			'store_email' => $storeDetial->store_email, 
			'address' => $storeDetial->address,
			'country' => $storeDetial->country,
			'country_code' => $storeDetial->country_code, 
			'phone' => $storeDetial->phone, 		 
			'deleted_at' => date('Y-m-d h:i:s'),
			'status' => '2'
		]);  
	}   

	public static function saveDefaultAppData($store_id){
		return true;
	} 

	public static function deleteDefaultAppData($store_id){
		$storeDetial = Store::select('*')->where(["store_id" => $store_id,"status" => '1'])->first();  
        if(!$storeDetial) {
            return false;
		} 
		 
		//Remove all Data
		User::where(['store_id' => $store_id])->delete(); 
		StorePaymentHistory::where(['store_id' => $store_id])->delete(); 
		Store::where(['store_id' => $store_id])->delete();  
	} 
	
	// Function for update Script JS Latest File
	public static function updateScriptJs($store_id) { 
		 $storeDetail = Store::where(['store_id' =>  $store_id,'status' => '1'])->get()->first();  
		 //cerate JS file 
		 $BcJsFileName = "big_".$storeDetail->id.time();  
		 $path = public_path();
		 $myfile = fopen("$path/bc_js/$BcJsFileName.js", "w") or die("Unable to open file!");
		 $txt = self::createScriptFile($storeDetail->id);
		 fwrite($myfile, $txt);
		 fclose($myfile);
		 chmod("$path/bc_js/$BcJsFileName.js", 0777); 
		 //Create dymamic js file end 
		 try{ 
            $clientId = config('constants.BC_APP_CLIENT_ID');
			$BcFilePath = self::getAssetURL().'bc_js/'.$BcJsFileName.'.js';
			$storeHashKey = $storeDetail->store_hash_key;
			$accessToken = $storeDetail->access_token; 
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.bigcommerce.com/stores/$storeHashKey/v3/content/scripts",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>"{\n  \"name\": \"Notification Wiz\",\n  \"description\": \"Build responsive websites 1\",\n  \"html\": \"<script>var not_wiz = {orderId: '{{checkout.order.id}}',customerId: '{{customer.id}}'}</script><script src='$BcFilePath'></script>\",\n  \"auto_uninstall\": true,\n  \"load_method\": \"default\",\n  \"location\": \"head\",\n  \"visibility\": \"all_pages\",\n  \"kind\": \"script_tag\",\n  \"consent_category\": \"essential\"\n}",
                CURLOPT_HTTPHEADER => array(
                    "store_hash: $storeHashKey",
                    "X-Auth-Client: $clientId",
                    "X-Auth-Token: $accessToken",
                    "Content-Type: application/json"
                ),
            ));
            $response = curl_exec($curl);
			curl_close($curl); 
			$json_response = json_decode($response,true); 
			
			if( ($json_response['data']) ){ 
				$fileName = $storeDetail->bc_jsfile.'.js';
				$path = public_path();
				$myFile = "$path/bc_js/$fileName";  
				if(file_exists($myFile)) {   
					chmod($myFile,0777);
					unlink($myFile);
				}				
				// end file code     
				$storeDetail->update([
					'bc_uuid' => $json_response['data']['uuid'],
					'bc_jsfile' => $BcJsFileName
				]);  
			} 
			
        }catch (RequestException $e) { 
            echo $e->getResponse(); die;
        } 
	}
   
	// Function for delete script JS
	public static function deleteScriptJs($store_id){ 
		 $storeDetail = Store::where(['store_id' =>  $store_id,'status' => '1'])->get()->first();   
		 if($storeDetail) { 
			$clientId = env('BC_APP_CLIENT_ID');
			$accessToken = $storeDetail->access_token;
			$hashKey =  $storeDetail->store_hash_key;
			$Uuid =  $storeDetail->bc_uuid;  
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.bigcommerce.com/stores/$hashKey/v3/content/scripts/$Uuid",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "DELETE",
				CURLOPT_HTTPHEADER => array(
					"store_hash: $hashKey",
					"X-Auth-Client: $clientId",
					"X-Auth-Token: ".$accessToken.""
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);  
			//delete js  file code
			$fileName = $storeDetail->bc_jsfile.'.js';
			$path = public_path();
			$myFile = "$path/bc_js/$fileName"; 

			if(file_exists($myFile)) {   
			   chmod($myFile,0777);
			   unlink($myFile);
			} 
			//end file code       
			$storeDetail->update(['bc_uuid' => '','bc_jsfile' => '']);
		 } 
	}  
	
	//Generate Frontend Append JS FILE Dyanmically on App install
	public static function createScriptFile($sId){
		ob_start();
        $storeDetial = Store::select('*')->where(["id" => $sId,"status" => '1'])->first();  
        if(!$storeDetial) {
            return false;
        } 
        $storeName = $storeDetial->store_name;
        $storeFrontUrl = $storeDetial->store_url;
        $storeHashKey = $storeDetial->store_hash_key;
        $appId = $storeDetial->store_id;  
        $currency_symbol = $storeDetial->currency_symbol; 
        $appUrl = env('APP_URL');  
        $currencySymbol = $storeDetial->currency;  
        ?>  
        (function(){ 
            var storeName = '<?php echo $storeName; ?>';
            var app_url = '<?php echo $appUrl; ?>';
            var store_url = '<?php echo $storeFrontUrl; ?>';
            var store_hash = '<?php echo $storeHashKey; ?>';
            var app_id = '<?php echo $appId; ?>'; 
            var currencySymbol = '<?php echo $currency_symbol; ?>';  
            var startingTime = new Date().getTime(); 
            var head = document.getElementsByTagName('head')[0];   
            var checkReady = function(callback) {
                if (window.jQuery) {
                    callback(jQuery);
                } else {
                    window.setTimeout(function() { checkReady(callback); }, 20);
                }
            };  
            checkReady(function($) {
                $(function() { 
                    
                });               
           })(); 
		});  
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}
 
}