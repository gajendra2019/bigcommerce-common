<?php 
/**
 * @author [gajendra]
 * @email [gajendra@bitcot.com]
 * @create date 2021-01-09 13:09:34
 * @modify date 2021-01-09 13:09:34
 * @desc [ common webohooks function collections ]
 */

namespace App\Utils;   

use Illuminate\Http\Request; 
use Illuminate\Http\Response;  
//GuzzleHttp
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client; 
//models
use App\Models\Store; 

class WebHookHelpers {   

	//function for get URL
	private static function getURL(){
		return env('APP_URL').'public/Bc_WebHook.php'; //Create this file on public folder Or can change the name
	} 
	
	//common function for validate web hook type exist OR not 
	private static function validateWebHookType($type){  
		$wh_types = array(  
			//Store Hooks
			'store/app/uninstalled',
			'store/information/updated',
			 //Customer Hooks
			'store/customer/*',
			'store/customer/created',
			'store/customer/updated',
			'store/customer/deleted',
			'store/customer/address/created',
			'store/customer/address/updated',
			'store/customer/address/deleted',
			'store/customer/payment/instrument/default/updated',
			//Categories Hooks
			'store/category/*',
			'store/category/created',
			'store/category/updated',
			'store/category/deleted', 
			//Product Hooks
			'store/product/*',
			'store/product/created',
			'store/product/updated',
			'store/product/deleted',
			'store/product/inventory/updated',
			'store/product/inventory/order/updated', 

			//Orders Hooks
			'store/order/*',
			'store/order/created',
			'store/order/updated',
			'store/order/archived',
			'store/order/statusUpdated',
			'store/order/message/created',
			'store/order/address/deleted',
			'store/order/refund/created',  
			//Cart
			'store/cart/*',
			'store/cart/created',
			'store/cart/updated',
			'store/cart/deleted',
			'store/cart/couponApplied',
			'store/cart/abandoned', 
			'store/cart/converted', 
			//Shipment
			'store/shipment/*',
			'store/shipment/created',
			'store/shipment/updated',
			'store/shipment/deleted',  
			//subscriber
			'store/subscriber/*',
			'store/subscriber/created',
			'store/subscriber/updated',
			'store/subscriber/deleted', 
		);

		if( in_array($type, $wh_types)) {
			return true;
		} else {
			return false;
		}
		 
	} 
	
	//Common function for get existing all web hooks detail
	public static function getAllWebHooks($store_id) { 
		$store = Store::where(['store_id' => $store_id ,'status' => '1'])->get()->first();
    	if(!$store){
    		return array('status' => false,'message' => 'Invalid Store Detail');
    	}   
    	$store_hash = $store->store_hash_key;
	    $auth_token = $store->access_token;   
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$store_hash/v2/hooks",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $auth_token"
			  ),
		)); 
		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
		  return array('status' => false,'message' => $err);
		} else { 
			return json_decode($response, true); 
		}
	}
    

    //common function for get single web hook detail
    public static function getOneWebHook($store_id, $wh_id ,$cb_func = false){
    	$store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first(); 
		if(!$store){
			return array('status' => false,'message' => 'Invalid Store Detail');
		}    

		$store_hash = $store->store_hash_key;
    	$auth_token = $store->access_token;    

		$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$store_hash/v2/hooks/$wh_id",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $auth_token"
			  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) { 
		  return array('status' => false ,'message' => $err);
		} else { 
			$response = json_decode($response,true);  
			if($cb_func) { return $response; } 
			else { return array('status' => true ,'wh_info' => $response); }
		}

    }
    
    //function Webhook create
    public static function createWebHook($store_id,$wh_type){    
    	$wh_type = trim($wh_type);
    	$store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first();
    	if(!$store){
    		return array('status' => false,'message' => 'Invalid Store Detail');
    	}  
    	$validte = self::validateWebHookType($wh_type);
        if(!$validte){
        	return array('status' => false,'message' => 'Invalid Web Hook Type');
        }   
        $webHooks =  self::getAllWebHooks($store_id); 
        if(!empty($webHooks)) {
        	foreach ($webHooks as $key => $wh) { 
	    		 if($wh['scope'] == $wh_type){
	    		 	return array('status' => true, 'message' => 'This web hooks already been exists','wh_info' => $wh);
	    		 } 
        	} 
        } 
    	$url = self::getURL(); 
    	$store_hash = $store->store_hash_key;
    	$auth_token = $store->access_token;  
    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$store_hash/v2/hooks",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\"scope\":\"$wh_type\",\"destination\":\"$url\",\"headers\":{}}",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $auth_token"
			  ),
		)); 
		$response = curl_exec($curl);
		$err = curl_error($curl);  
		curl_close($curl); 
		if ($err) {
		  return array('status' => false, 'message' => $err);
		} else { 
			$response = json_decode($response , true);
			return array('status' => true, 'message' => 'Web hook created successfully','wh_info' => $response);  
		} 
    	 
    }    

    //function for update specific web hook
    public static function updateWebhook( $store_id, $wb_id ,$updatefields = array()) { 
    	$store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first(); 
		if(!$store){
			return array('status' => false,'message' => 'Invalid Store Detail');
		}    
		$webHook =  self::getOneWebHook($store_id , $wb_id, true); //get all web hooks list 
    	if( (isset($webHook['status'])) && ($webHook['status'] == 404) ) {
    		return array('status' => false,'message' => $webHook['title']);
    	}
 
    	$fields_string = json_encode($updatefields); 
    	$wh_id = $webHook['id']; 
    	$store_hash = $store->store_hash_key;
    	$auth_token = $store->access_token;  
    	$curl = curl_init();  
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$store_hash/v2/hooks/$wh_id",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PUT",
			  CURLOPT_POSTFIELDS => "$fields_string",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $auth_token"
			  ),
		)); 		 
		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
			return array('status' => false,'message' => $err);  
		}  else {
			$response = json_decode($response,true);
			if( isset($response['status'])) {
				return array('status' => false,'message' => 'Webhook update issue','response' => $response);  
			}else {
		    	return array('status' => true,'message' => 'Webhook updated successfully','wh_info' => $response);  
			}
		}  
    } 
    
    //Function for delete all web hooks of specific Store
    public static function deleteAllWebHooks($store_id) {   		 
		$store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first(); 
		if(!$store){
			return array('status' => false,'message' => 'Invalid Store Detail');
		}    
		$webHooks =  self::getAllWebHooks($store_id); //get all web hooks list
		$store_hash = $store->store_hash_key;
    	$auth_token = $store->access_token;    
        if(!empty($webHooks)) {
        	foreach ($webHooks as $key => $wh) {  
        		 $wh_id = $wh['id'];
        		 $curl = curl_init(); 
				 curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://api.bigcommerce.com/stores/$store_hash/v2/hooks/$wh_id",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "DELETE",
					  CURLOPT_HTTPHEADER => array(
					    "accept: application/json",
					    "content-type: application/json",
					    "x-auth-token: $auth_token"
					  ),
				)); 
				$response = curl_exec($curl);
				$err = curl_error($curl); 
				curl_close($curl); 
				if ($err) { 
				  return array('status' => false, 'message' => $err);
				}   
        	}  
        	$response = json_decode($response , true);
			return array('status' => true, 'message' => 'All webhooks deleted successfully');
        }
    } 

    //function for delete specific web hook
    public static function deleteOneWebHook( $store_id, $wb_id ) { 
    	$store = Store::where(['store_id' => $store_id,'status' => '1'])->get()->first(); 
		if(!$store){
			return array('status' => false,'message' => 'Invalid Store Detail');
		}    
		$webHook =  self::getOneWebHook($store_id , $wb_id, true); //get all web hooks list 
    	if( (isset($webHook['status'])) && ($webHook['status'] == 404) ) {
    		return array('status' => false,'message' => $webHook['title']);
    	}

    	//echo '<pre>';print_r($webHook);die; 
    	$wh_id = $webHook['id']; 
    	$store_hash = $store->store_hash_key;
    	$auth_token = $store->access_token; 

    	$curl = curl_init(); 
		curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.bigcommerce.com/stores/$store_hash/v2/hooks/$wh_id",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "DELETE",
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "content-type: application/json",
			    "x-auth-token: $auth_token"
			  ),
		)); 
		$response = curl_exec($curl);
		$err = curl_error($curl); 
		curl_close($curl); 
		if ($err) {
			return array('status' => false,'message' => $err);  
		}  else {
			$response = json_decode($response,true);
			return array('status' => true,'message' => 'Webhook deleted successfully','wh_info' => $response); 
		}  
    }
     
 
}