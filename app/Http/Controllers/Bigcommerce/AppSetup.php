<?php

namespace App\Http\Controllers\Bigcommerce;
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Http\Response; 
use Carbon\Carbon;  
//Spatie
use Spatie\Permission\Models\Role; 

//GuzzleHttp
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client; 

//Models
use App\Models\Store;
use App\Models\User;

//Utils Libraries
use App\Utils\BcHelper;
use App\Utils\WebHookHelpers;
use App\Utils\EmailHelper;

class AppSetup extends Controller
{ 
    protected $baseURL;

    public function __construct()
    { 
        $this->baseURL = env('APP_URL');
        if(substr($this->baseURL , -1) != '/' ){
            $this->baseURL.='/';
        } 
    }

    /**
     * This function for get app client key
     */
    public function getAppClientId() {
        return config('constants.BC_APP_CLIENT_ID');
    }

    /**
     * This fucntion for get app secret
     */
    public function getAppSecret() { 
         return config('constants.BC_APP_SECRET');
    } 

    /** 
     * This function for when error on install/load app
     */
    public function error(Request $request) {
        $errorMessage = "Internal Application Error"; 
        if ($request->session()->has('error_message')) {
            $errorMessage = $request->session()->get('error_message');
        } 
        echo '<h4>An issue has occurred:</h4> <p>' . $errorMessage . '</p> <a href="'.$this->baseURL.'">Go back to home</a>';
    } 

    /**
     * This function for install all defined webhooks
     */
    private function installWebHooks($store_id){
        $wh_array = array(
            // 'store/information/updated',
            // 'store/product/created',
            // 'store/product/updated',
            // 'store/product/inventory/updated',
            // 'store/product/deleted',
            // 'store/order/created', 
            // 'store/order/updated',                    
            // 'store/order/statusUpdated', 
            // 'store/order/refund/created',
            // 'store/order/archived',
        );
        if(count($wh_array)) {
            foreach ($wh_array as $key => $value) { 
                $result = WebHookHelpers::createWebHook($store_id,$value); 
            } 
        } 
    }      

    /*
    * This function use when APP install
    */
    public function appInstall(Request $request) { 
        $get = $request->all(); 
        if (!$request->has('code') || !$request->has('scope') || !$request->has('context')) {
            return $this->error($request);
        }
        try {    
            $client = new Client();
            $result = $client->request('POST', 'https://login.bigcommerce.com/oauth2/token', [
                'json' => [
                    'client_id' => $this->getAppClientId(),
                    'client_secret' => $this->getAppSecret(),
                    'redirect_uri' => $this->baseURL.'auth/install',
                    'grant_type' => 'authorization_code',
                    'code' => $request->input('code'),
                    'scope' => $request->input('scope'),
                    'context' => $request->input('context'),
                ]
            ]); 
            $statusCode = $result->getStatusCode();
            $data = json_decode($result->getBody(), true);
            $userModel = null;                     
            if ($statusCode == 200) {   
                //Default App Data Delete
                $storeHashKey = explode("/",$data['context'])[1];
                BcHelper::deleteDefaultAppData($storeHashKey); //If Unistall hook not work then it will delete previous data 
                $storeResult = BcHelper::createStoreAndClient($data); //create store & login user in DB  
                if( $storeResult['status'] == false) {
                    return redirect('https://login.bigcommerce.com/app/' . $this->getAppClientId() . '/install/failed');
                } else {   
                    $userModel = $storeResult['user']; //get user model 
                    $this->installWebHooks($userModel->store_id);
                    BcHelper::saveDefaultAppData($userModel->store_id);//save default setting if any 
                    EmailHelper::appInstall($userModel->store_id); //send welcome email
                    if ($request->has('external_install')) {
                        return redirect('https://login.bigcommerce.com/app/' . $this->getAppClientId() . '/install/succeeded');
                    }  
                } 
            }   
            return redirect('/dashboard/auth?store_user='.$userModel->id);
        } catch (RequestException $e) {
            $statusCode = $e->getResponse()->getStatusCode();
            $errorMessage = "An error occurred.";
            if ($e->hasResponse()) {
                if ($statusCode != 500) {
                    $errorMessage = Psr7\str($e->getResponse());
                }
            }
            
            if ($request->has('external_install')) {
                return redirect('https://login.bigcommerce.com/app/' . $this->getAppClientId() . '/install/failed');
            } else {
                echo 'error : '.$errorMessage; die;
            }
        }
    }

    /*
    * This function use when APP uninstall
    */
    public function appUnInstall(Request $request)
    {
        $signedPayload = $request->input('signed_payload');
        $verifiedSignedRequestData = $this->verifySignedRequest($signedPayload, $request); 
        $store_hash = $verifiedSignedRequestData['store_hash']; 
        $storeData = Store::where(['store_hash_key' => $store_hash,'status' => '1'])->get()->first(); 
        if($storeData){  
            WebHookHelpers::deleteAllWebHooks($store_hash); //delete all webhooks
            EmailHelper::appInstall($store_hash); //send email
            BcHelper::deleteScriptJs($store_hash); //delete script  
            BcHelper::appUnInstallLog($store_hash); //manage store uninstall log   
            BcHelper::deleteDefaultAppData($store_hash); //delete store data    
        }  
    }

    /*
    * This function use when APP load
    */
    public function appLoad(Request $request)
    {
        $signedPayload = $request->input('signed_payload');  
        if (!empty($signedPayload)) {
            $verifiedSignedRequestData = $this->verifySignedRequest($signedPayload, $request);  
            if ($verifiedSignedRequestData !== null) {  
                $store_hash = $verifiedSignedRequestData['store_hash'];  
                $store = Store::where(['store_id' => $store_hash,'status' => '1'])->get()->first();
                if($store){ 
                    $userModel = User::where(['store_id' => $store->store_id,'status' => '1'])->get()->first();
                    if($userModel) { 
                        return redirect('/dashboard/auth?store_user='.$userModel->id); 
                    } 
                } 
                //set app id in local storage  
                echo '<h5>We are not able to load your dashboard, due to app not properly setup</h5>';die;
               // return redirect('/app/dashboard/'.$appId.''); 
            } else {
                return view('error');
            }
        } else {
            return view('error');
        } 
    }

    /** 
     * This function for when remove user
     */
    public function removeUser(Request $request){
        return true;
    }

    /**
     * This fucntion for verify payload
     */
    private function verifySignedRequest($signedRequest, $appRequest) {   
        list($encodedData, $encodedSignature) = explode('.', $signedRequest, 2);   
        $signature = base64_decode($encodedSignature);
        $jsonStr = base64_decode($encodedData);
        $data = json_decode($jsonStr, true);   
        $expectedSignature = hash_hmac('sha256', $jsonStr, $this->getAppSecret($appRequest), $raw = false);
        if (!hash_equals($expectedSignature, $signature)) {
            error_log('Bad signed request from BigCommerce!');
            return null;
        } else { 
            return $data; 
        } 
    } 

    //This function is only for testing purpose ( display all running webhooks)
    public function webhooks(Request $request,$store_id) { 
        $store = Store::where(['store_id' => $store_id,'status' => '1']); 
        if(!$store){
            echo '<pre>';print_r([
                'status' => false,
                'message' => 'Invalid Store Detail'
            ]);die;
        }   
        //self::installWebHooks($store_id);die;
        $getAllWh =  WebHookHelpers::getAllWebHooks($store_id);
        echo '<pre>';print_r($getAllWh);die; 
    }
}
