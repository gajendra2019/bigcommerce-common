<?php

namespace App\Http\Controllers\Bigcommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response; 
 
//Models
use App\Utils\BcApiHelper;
use App\Models\Store;

//Utils Libraries
use App\Utils\EmailHelper;
 

class StripeWebHookController extends Controller
{

    public function __construct()
	{
	    $this->baseURL = env('APP_URL');
	}


	public function stripeWebHook(Request $request) {
         $post = $request->all(); 
         //return $post['data']['object'];
         if( isset($post['type'])) { 
         	 $type = $post['type']; 
	         switch ($type) {
	         	case 'customer.subscription.updated':
	         		 return $this->subscriptionUpdated($post['data']['object']);
	         		break;         	
	         	default:
	         		# code...
	         		break;
	         }  
         } 
    }  

    
    //Webhook subscriptiom update( callback function )
    private function subscriptionUpdated($data) {  
         if(isset($data['id'])) {
         	 $current_period_start = date( 'Y-m-d H:i:s',($data['current_period_start']) );
	         $current_period_end = date( 'Y-m-d H:i:s',($data['current_period_end']));
	         $sub_id = $data['id']; 
	         $findStore = Store::where(['stripe_sub_id' => $sub_id,'status' => '1'])->get()->first();
	         if($findStore) { 
				$findStore->update(['start_date' => $current_period_start,'end_date' => $current_period_end]); 
				if( isset($data['status']) && ($data['status'] == 'canceled') ) {
					EmailHelper::planCancelled($store->store_id);//send email for plan cancelled
				}  
	         } 
	         return response()->json([
	         	'status' => true,
	         	'message' => 'Subscription Updated Webhook Executed Successfully'
	         ]);
         }  
    } 


}
