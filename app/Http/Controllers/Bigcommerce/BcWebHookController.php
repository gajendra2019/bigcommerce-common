<?php

namespace App\Http\Controllers\Bigcommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
//GuzzleHttp
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client; 

//BC Client Library 
use Bigcommerce\Api\Client as Bigcommerce;  

use App\Utils\BcApiHelper;
use App\Models\Store;


class BcWebHookController extends Controller
{

    public function __construct()
	{
	    $this->baseURL = env('APP_URL');
        if(substr($this->baseURL , -1) != '/' ){
            $this->baseURL.='/';
        } 
	}


	public function getAppClientId() {
        if (env('APP_ENV') === 'local') {
            return env('BC_LOCAL_CLIENT_ID');
        } else {
            return env('BC_APP_CLIENT_ID');
        }
    }

    public function getAppSecret(Request $request) {
        if (env('APP_ENV') === 'local') {
            return env('BC_LOCAL_SECRET');
        } else {
            return env('BC_APP_SECRET');
        }
    } 


     
    public function bcWebhooks(Request $request) {
        $post = $request->all(); 
        $validator = Validator::make($post,[
            'store_id' => ['required'],
            'producer' => ['required'],
            'scope' => ['required'],
            'hash' => ['required'],
            'type' => ['required'],
            'id' => ['required']
        ]); 
        if($validator->fails()) {
            $allMessages = $validator->messages();
            $result = errorArrayCreate($allMessages);
            return response()->json([
                'status' => false,
                'message' => 'No Get Proper values',
                'errors' => $result
            ]);
        }else{ 
            //return $post;
            $storeID = explode('/', $post['producer'])[1];  
            $store = Store::where(['store_id' => $storeID,'status' => '1'])->get()->first();
            if(!$store){
                return response()->json([
                    'status' => false,
                    'message' => 'No Store Associated with this ID'
                ]);
            }  
            $id = $post['id'];  
            if($store) {  
                switch ($post['scope']) {
                    case 'store/information/updated':
                        return $this->storeInfoUpdated($store);
                        break;
                    case 'store/product/created': 
                    case 'store/product/updated':
                    case 'store/product/inventory/updated':
                        return $this->productCreateUpdate($store, $id);
                        break;
                    case 'store/product/deleted':
                        return $this->productDelete($store, $id);
                        break; 
                    case 'store/order/created': 
                    case 'store/order/updated':                    
                    case 'store/order/statusUpdated': 
                    case 'store/order/refund/created':
                        return $this->createUpdateOrder($store, $id);
                        break;
                    case 'store/order/archived':
                        return $this->deleteOrder($store, $id);
                        break;        
                    default:
                        # code...
                        break;
                } 
            }   

        }  
    }



    //Updated Store Detail
    private function storeInfoUpdated($store){
        $storeDetail = BcApiHelper::getStoreDetail($store->store_hash_key,$store->access_token);        
        if( isset($storeDetail['data']['id']) ) {
            $storeDetail = $storeDetail['data'];        
            $updated = Store::where(['store_id' => $store->store_id])
                    ->update([ 
                        'store_name' => $storeDetail['name'],
                        'store_email' => $storeDetail['admin_email'], 
                        'address' => $storeDetail['address'],
                        'country' => $storeDetail['country'],
                        'country_code' => $storeDetail['country_code'], 
                        'phone' => $storeDetail['phone'], 
                        'store_url' => $storeDetail['secure_url'],
                        'control_panel_base_url' => $storeDetail['control_panel_base_url'],
                        'store_logo_url' => isset($storeDetail['logo']['url'])?$storeDetail['logo']['url']:null,
                        'order_email' => $storeDetail['order_email'],
                        'currency' => $storeDetail['currency'],
                        'currency_symbol' => $storeDetail['currency_symbol'],
                    ]);
            if($updated){
                return response()->json([
                    'status' => true,
                    'message' => 'Store detail updated successfully'
                ]);
            }  
        } else {
            return response()->json($storeDetail['data']);
        } 
    }  
}
