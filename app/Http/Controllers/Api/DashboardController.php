<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;


use App\Models\User;
use App\Models\Store; 
 

class DashboardController extends Controller
{  
        
	/** 
	* Logout user (Revoke the token)
	*
	* @return \Illuminate\Http\JsonResponse
	* 
	*/
    public function getDashboard(Request $request)
    {
        return response()->json([
            'status' => true,
            'data' => array()
        ]);
    }

   

    
}
