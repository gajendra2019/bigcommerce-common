<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;


use App\Models\User;
use App\Models\Store; 
 

class AuthController extends Controller
{ 
 
    //Function of auto login user
    public function login( Request $request ) {   
        $post = $request->all(); 
        $validator = Validator::make($request->all(),[
			'store_user' => 'required' 
    	]);  
        if($validator->fails()) { 
            return response()->json([
                'status' => false,
                'message' => 'Opps! Invalid store user ID' 
            ]);            
        } 

       $userModel = User::where(['id' => intval($post['store_user']),'status' => '1'])->get()->first(); 
       if(!$userModel) {
            return response()->json([
                'status' => false,
                'message' => 'Opps! You are trying to invalid store login.' 
            ]);  
       }  
        
        if (auth()->attempt(['email' => $userModel->email,'password' => $userModel->store_id])) {
            $user = auth()->user();
            $token = $user->createToken('search-product')->accessToken;   
            $store = Store::with('planInfo')->where(['store_id' => $userModel->store_id])->get()->first(); 
            
            $planStatus = false;
            $planExpireOn = null;
            $planName = null;
            if($store) {
                $now = Carbon::now();              
                $planExpireOn = Carbon::parse($store->end_date);  
                $planStatus = true; 
                $planName = isset($store->planInfo->plan_name)?$store->planInfo->plan_name:'Free Demo';
                if( ($planExpireOn->lessThan($now)) == true) { 
                    $planStatus = false; 
                }  
            }   
            return response()->json([
                'status' => true,               
                'token_type' => 'Bearer',
                'token' => $token,
                'storeUser' => $userModel->id,
                'user' => array(
                    'id' => $userModel->id,
                    'name' => $userModel->name,
                    'email' => $userModel->email,
                    'phone' => $userModel->phone, 
                    'store_id' => $userModel->store_id,
                    'store_hash' => isset($store->store_hash_key)?$store->store_hash_key:null,
                    'store_name' => isset($store->store_name)?$store->store_name:null,                    
                ),
                'plan' => array(
                    'plan_name' => $planName,
                    'current_status' => $planStatus,
                    'next_payment' => $planExpireOn 
                )
            ]);
        }else {
            return response()->json(['status' => false, 'message' => 'Unauthorized User.']);
        }   
    }

        
	/** 
	* Logout user (Revoke the token)
	*
	* @return \Illuminate\Http\JsonResponse
	* 
	*/
    public function logout(Request $request)
    {
        if(!Auth::guest()) {
            Auth::user()->token()->revoke();
            return response()->json([
                'status' => true,
                'message' => 'Successfully logged out.'
            ]);
        } 
    }

   

    
}
