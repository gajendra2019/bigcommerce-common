<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon; 

use App\Models\User;
use App\Models\Store; 
use App\Models\StoreAppStatus;  
use App\Utils\BcHelper;

class StoreAppStatusController extends Controller
{
    public function index(Request $request) {
        $data = StoreAppStatus::select('id','is_show','is_setting_updated')
                ->where(['store_id' => Auth::user()->store_id])->get()->first();
        return response()->json([
            'status' => ($data)?true:false,
            'data' => $data
        ]);
     }

     public function save(Request $request) {
         $post = $request->all(); 
         $validator = Validator::make($post,[
            'id' => ['required'],
            'is_show' => ['required'] 
        ]); 
        if($validator->fails()) {
            $allMessages = $validator->messages();
            $result = errorArrayCreate($allMessages);
            return response()->json([
                'status' => false,
                'message' => 'Form not fillup proper values',
                'errors' => $result
            ]);
        }else{                  
                $saveData = array(
                    'store_id' => (Auth::user()->store_id),
                    'is_show' => ($post['is_show'] === true)?'1':'0' 
                );  
                 //BcHelper::updateScriptJs(Auth::user()->store_id);
                $result = StoreAppStatus::updateOrCreate(
                    array('store_id' => Auth::user()->store_id,'id' => $post['id']),
                    $saveData
                );  
                if( $result ) {
                    return response()->json([
                        'status' => true,
                        'message' => 'Your app status updated successfully' 
                    ]); 
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Opps! getting error while updating' 
                    ]);
                }  
        } 
         
     }
     
     public function applyAppChanges(Request $request) {         
        $post = $request->all();

        if(isset($post['app_status']) &&  ($post['app_status'] == '1') ) { 

            //BcHelper::updateScriptJs(Auth::user()->store_id);
            StoreAppStatus::where(['store_id' => Auth::user()->store_id])
                      ->update(['is_setting_updated' => '0']); 
            return response()->json([
                'status' => true,
                'message' => 'Your changes applied successfully'
            ]); 
        } else {
            return response()->json([
                'status' => true,
                'message' => 'Already no changes found to update'
            ]);
        } 
     }
}
