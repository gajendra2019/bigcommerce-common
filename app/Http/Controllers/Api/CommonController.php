<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;  
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;

//Models
use App\Models\User;
use App\Models\Store; 

//Utils Libraries
use App\Utils\EmailHelper;
 

class CommonController extends Controller
{  
        
	/** 
	* Logout user (Revoke the token)
	*
	* @return \Illuminate\Http\JsonResponse
	* 
	*/
    public function getStoreProfile(Request $request)
    {
        $storeProfile = Store::with(['planInfo' => function($model){
            $model->select('id','plan_id','plan_name');

        }])->select('store_id','store_email','access_token','store_name','address','country','country_code','phone','store_url','control_panel_base_url',
        'currency','currency_symbol','end_date','stripe_plan_id')
        ->where(['store_id' => Auth::user()->store_id])->get()->first();
        return response()->json([
            'status' => true,
            'data' => $storeProfile
        ]);
    }


    public function contactSupport(Request $request) { 
        $post = $request->all();
        $validator = Validator::make($post,[ 
           'contact_subject' => ['required'],
           'contact_message' => ['required'] 
        ]); 
        if($validator->fails()) {
           $allMessages = $validator->messages();
           $result = errorArrayCreate($allMessages);
           return response()->json([
               'status' => false,
               'message' => 'Form not fillup proper values',
               'errors' => $result
           ]);
        }else{    

          $store = Store::where(['store_id' => Auth::user()->store_id])->get()->first(); 
          $support = \App\Models\AppSupport::get()->first(); 
          
          $cSupport = new \App\Models\StoreContactSupport();
          $saved = $cSupport->create([
                'store_id' => Auth::user()->store_id,
                'sender_name' => $store->store_name,
                'sender_email' => $store->store_email,
                'sender_subject' => trim($post['contact_subject']),
                'message' => trim($post['contact_message']),
                'receiver_email' => $support->app_support_email
          ]); 
          if($saved) {
                 //Send email to Admin
                 EmailHelper::contactSupportAdmin($store->store_id,$saved->id); 
                 //Send email to store owner
                 EmailHelper::contactSupportClient($store->store_id,$saved->id);
                return response()->json([
                    'status' => true,
                    'message' => 'Your contact query submitted successfully'
                ]);
          } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Facing some issue in save contact support data'
                ]);
          }
         
       } 
    }

   

    
}
