<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

//Models
use App\Models\User;
use App\Models\Store; 
use App\Models\Plans;
use App\Models\StorePaymentHistory;

//Utils Libraries
use App\Utils\StripeHelper;
use App\Utils\EmailHelper;

class StripeController extends Controller
{ 
    public function subscriptionPlans(Request $request) {
        $plans = Plans::where(['status' => '1'])->get()->all();
        return response()->json([
            'status' => true,
            'data' => $plans
        ]);
    }

    public function subscriptionPlan(Request $request,$id) {
        $plan = Plans::where(['status' => '1','id' => $id])->get()->first();
        if(!$plan){
            return response()->json([
                'status' => false,
                'message' => 'Invalid plan you have selected'
            ]);

        } else {
            return response()->json([
                'status' => true,
                'data' => $plan
            ]);
        }

    }

    public function subscriptionCreate(Request $request) { 
        $post = $request->all();
        $post = $request->all();
        $validator = Validator::make($post,[ 
            'stripe_token' => ['required'],
            'plan_id' => ['required'] 
        ]);
        if($validator->fails()) {
            $allMessages = $validator->messages();
            $result = errorArrayCreate($allMessages);
            return response()->json([
                'status' => false,
                'message' => 'Please fill up proper form values',
                'errors' => $result
            ]);
        }else{  

            $store_id = Auth::user()->store_id;

            $store = Store::where(['store_id' => $store_id])->get()->first();
            if(!$store){
                return response()->json([
                    'status' => false,
                    'message' => 'Sorry to activate plan : invalid store id' 
                ]);
            } 
            $plan = Plans::where(['plan_id' => $post['plan_id']])->get()->first();
            if(!$plan){
                return response()->json([
                    'status' => false,
                    'message' => 'Sorry to activate plan : invalid plan id' 
                ]);
            }
            //Created customer
            $result = StripeHelper::customer($store_id,$post['stripe_token']);
            if($result['status'] == false) { 
                return response()->json($result); 
            }

            //Created Subscription
            $result = StripeHelper::plan($result['customer_id'],$post['plan_id'],$store_id);

            if( isset($result['status'])  && ($result['status'] == true) ) {
                $store = Store::with('planInfo')->where(['store_id' => $store_id])->get()->first();
                $result['subscription'] = array(
                    'plan_name' => isset($store->planInfo->plan_name)?$store->planInfo->plan_name:'Free Demo',
                    'current_status' => true,
                    'next_payment' => $store->end_date 
                ); 
                //Save Payment History
                StripeHelper::save_payment_history($store->stripe_sub_id,$store->store_id);
                EmailHelper::planActivation($store->store_id);//send email for plan activation 

                 //Email Code Here<TODOs>

            } 
            return response()->json($result);
        } 
    } 

    public function getPlanStatus(Request $request,$store_id){
        $store = Store::where(['store_id' => $store_id])->get()->first();
        if(!$store){
            return response()->json([
                'status' => false,
                'message' => 'Sorry to activate plan : invalid store id' 
            ]);
        }  

        $planStatus = false; 
        if($store) {
            $now = Carbon::now();              
            $planExpireOn = Carbon::parse($store->end_date);  
            $planStatus = true;  
            if( ($planExpireOn->lessThan($now)) == true) { 
                $planStatus = false; 
            }  
        }   

        return response()->json([
            'status' => false,
            'plan_status' => $planStatus 
        ]);
    }     
}
