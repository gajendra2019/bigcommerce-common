<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SPAController extends Controller
{
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index()
    {
        return view('vue-app');
    } 

    
    public function faq()
    {
        return view('public.faq');
    }


    public function privacyPolicy()
    {
        return view('public.privacy-policy');
    }


    public function termsOfService()
    {
        return view('public.terms-of-services');
    }
}
