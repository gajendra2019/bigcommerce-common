<?php

namespace App\Http\Middleware;


use Closure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 


class VerifyPlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    { 
        if(Auth::user()) {
            $store_id = Auth::user()->store_id;
            $store = \App\Models\Store::select('store_id','store_name','start_date','end_date')->where(['store_id' => $store_id])->get()->first(); 
            $now = Carbon::now();              
            $planExpireOn = Carbon::parse($store->end_date); 
            if( ($planExpireOn->lessThan($now)) == true) { 
                return response()->json( [ 'error' => 'Plan Expired' ], 403 ); 
            } else {
                return $next($request);
            } 
        }  
        return $next($request);
    }
}
