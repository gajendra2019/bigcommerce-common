<?php 
try{       
     $server_url = 'https://hubifyapps.com/bigcommerce/profit-insights/api/bc_webhook'; 
     $get_contents = file_get_contents('php://input');  
     $post = json_decode($get_contents,true);  
     //$myfile = fopen("/var/www/html/hubifyapps.com/bigcommerce/profit-insights/public/webhookkit/webhook.txt", "w") or die("Unable to open file!");
     //fwrite($myfile, print_r($post, TRUE)); 
     //fclose($myfile);  die; 
    $curl = curl_init(); 
    curl_setopt_array($curl, array(
        CURLOPT_URL => $server_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array(
            'store_id' => $post['store_id'],
            'producer' => $post['producer'],
            'scope' => $post['scope'],
            'hash' => $post['hash'],
            'type' => $post['data']['type'],
            'sku' => isset($post['data']['sku'])?$post['data']['sku']:'',
            'id' => isset($post['data']['id'])?$post['data']['id']:0
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    // echo $response;

}catch (Exception $e) { 
    return [
        'status' => false,
        'message' => $e->getMessage()
    ];
}
