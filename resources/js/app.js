// require('./bootstrap');
import Vue from 'vue';
import App from './app/components/AppComponent.vue'; 
import router from './app/routers/router';
import store from './app/store/index';
import config from './config';
import './http-common';  

//Vue Bootstrap
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';  
import 'bootstrap-vue/dist/bootstrap-vue.css'; 
import 'bootstrap/dist/css/bootstrap.css'; 
Vue.use(BootstrapVue); 
Vue.use(IconsPlugin); 

//Vue Toastr
import VueToastr from "vue-toastr";
Vue.use(VueToastr, {
    defaultTimeout: 3000,
    defaultProgressBar: false,
    defaultProgressBarValue: 0,
    defaultType: "success",
}); 

//Vue Datatable
import { ServerTable, ClientTable, Event } from 'vue-tables-2';
Vue.use(ClientTable); Vue.use(ServerTable); 

//Vue MultiSelect 
import Multiselect from 'vue-multiselect'
import "vue-multiselect/dist/vue-multiselect.min.css"
Vue.component('multiselect', Multiselect);

//full loader
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css'; 
Vue.use(Loading,{ color: '#fff', opacity: 0.9, backgroundColor : '#191212', zIndex : 999999 });
Vue.component('loading', Loading);

//Client Side Form Validation 
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import { messages } from 'vee-validate/dist/locale/en.json'; 
Object.keys(rules).forEach(rule => {
    extend(rule, {
        ...rules[rule], // copies rule configuration
        message: messages[rule] // assign message
    });
});
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

//Layputs Import
import AuthLayout from './app/components/layouts/AuthLayout.vue';
import StepLayout from './app/components/layouts/StepLayout.vue';
import StepLayoutPlans from './app/components/layouts/StepLayoutPlans.vue';
import SinglePageLayout from './app/components/layouts/SinglePageLayout.vue';
Vue.component("authLayout", AuthLayout);
Vue.component("stepLayout", StepLayout);
Vue.component("stepLayoutPlans", StepLayoutPlans);
Vue.component("spLayout", SinglePageLayout);


//Instantiate Vue
const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
