import axios from "axios";
import config from "./config"; 

const isHandlerEnabled = (config = {}) => {
    return config.hasOwnProperty("handlerEnabled") && !config.handlerEnabled
        ? false
        : true;
};

axios.interceptors.request.use(request => requestHandler(request)); 
const requestHandler = request => { 
    if (!!(localStorage.getItem(config.NAME_PREFIX+"token"))) {
        if (isHandlerEnabled(request)) {
            request.headers["Authorization"] =
                "Bearer " + localStorage.getItem(config.NAME_PREFIX+"token");
        }
    }
    return request;
};


axios.interceptors.response.use(
    response => successHandler(response),
    error => errorHandler(error)
);

const successHandler = response => {
    if (isHandlerEnabled(response)) {
        if (response.data.type === "success") {
            if (response.data.message) {
            }
        } else {
            if (response.data.message) {
            }
        }
    }
    return response;
};

const errorHandler = error => {
    if (isHandlerEnabled(error)) {
        if (typeof error.response !== "undefined") { 
            if (error.response.status === 401) {
                if (!!(localStorage.getItem(config.NAME_PREFIX+"token")) ) {
                    localStorage.removeItem(config.NAME_PREFIX+'token');
                    localStorage.removeItem(config.NAME_PREFIX+'storeUser');
                    localStorage.removeItem(config.NAME_PREFIX+'plan');
                    localStorage.removeItem(config.NAME_PREFIX+'user');
                    window.location.href = (config.BASE_URL+'dashboard/auth');
                }
            } else if( (error.response.status === 403) && (typeof(error.response.data.error)!='undefined') && (error.response.data.error == 'Plan Expired') ){
                //alert(config.BASE_URL+'plans')
                 window.location.href = (config.BASE_URL+'plans');
            }
        }
    }
};
