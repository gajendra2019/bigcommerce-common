import Vue from "vue";
import axios from "axios"; 

import config from "../../../config"; 

export default {
    login({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .post(config.API_URL + "login", payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return reject(error.response);
                });
        });
    },
    profile({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .get(config.API_URL + "profile", payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return reject(error.response);
                });
        });
    },
    dashboard({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .get(config.API_URL + "dashboard", payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return reject(error.response);
                });
        });
    },
    logout({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .get(config.API_URL + "logout")
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return reject(error.response);
                });
        });
    }, 
    //Stripe Plans
    getSubscriptionPlans({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .get(config.API_URL + "getSubscriptionPlans", payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return resolve(error.response);
                });
        });
    },
    getSubscriptionPlan({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .get(config.API_URL + "getSubscriptionPlan/"+payload.id)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return resolve(error.response);
                });
        });
    },
    subscriptionCreate({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .post(config.API_URL + "subscriptionCreate",payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return resolve(error.response);
                });
        });
    },
    getAppStatus({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .get(config.API_URL + "getAppStatus")
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return resolve(error.response);
                });
        });
    },
    saveAppStatus({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .post(config.API_URL + "saveAppStatus",payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return resolve(error.response);
                });
        });
    },  
    applyAppChanges({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .post(config.API_URL + "applyAppChanges",payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return resolve(error.response);
                });
        });
    },   
    contactSupport({ commit, state }, payload) {
        return new Promise(function (resolve, reject) {
            axios
                .post(config.API_URL + "contactSupport",payload)
                .then(function (response) {
                    return resolve(response.data);
                })
                .catch(function (error) {
                    return resolve(error.response);
                });
        });
    }

};