import Vue from "vue";
import VueRouter from "vue-router"; 
import config from "../../config";
Vue.use(VueRouter);   

//Route Compontets import
import Login from "../components/views/auth/Login.vue";
import Profile from "../components/views/profile/Profile.vue"; 
import Dashboard from "../components/views/dashboard/Dashboard.vue";

//app-instructions
import AppInstructions from "../components/views/app-instruction/AppInstructions.vue";  
//contact-us
import ContactSupport from "../components/views/contact-support/ContactSupport.vue";  

import PlansList from "../components/views/plans/PlansList.vue"; 
import PlanPayment from "../components/views/plans/PlanPayment.vue"; 

import Table from '../components/views/table/Table.vue';
import Form from '../components/views/form/Form.vue'; 
  

const router = new VueRouter({
    base: config.URL_PREFIX,
    mode: "history",
    linkExactActiveClass: "active",
    routes: [   
        {
            name: "Login",
            path: "/dashboard/auth",
            component: Login,
            meta: { requiresAuth: false, title : 'Login',layout : 'spLayout' }
        },
        {
            name: "dashboard",
            path: "/dashboard",
            component: Dashboard,
            meta: {  requiresAuth: true, title : 'Dashboard' }
        }, 
         //app-instructions
        {
            name: "AppInstructions",
            path: "/app-instructions",
            component: AppInstructions,
            meta: {  requiresAuth: true, title : 'App Instructions' }
        },         
        //contact us
        {
            name: "ContactSupport",
            path: "/contact-support",
            component: ContactSupport,
            meta: {  requiresAuth: true, title : 'Contact Support' }
        },   
        {
            name: "Profile",
            path: "/profile",
            component: Profile,
            meta: { requiresAuth: true, title : 'Profile' }
        },
        {
            name: "plansList",
            path: "/plans",
            component: PlansList,
            meta: { requiresAuth: true, title : 'Plans List',layout : 'stepLayoutPlans'}
        }, 
        {
            name: "paymentProcess",
            path: "/payment-process",
            component: PlanPayment,
            meta: { requiresAuth: true, title : 'Payment Process',layout : 'stepLayoutPlans' }
        },  
        {
            name: "Table",
            path: "/table",
            component: Table,
            meta: { requiresAuth: true, title : 'Table' }	
		},
		{
            name: "Form",
            path: "/form",
            component: Form,
            meta: { requiresAuth: true, title : 'Form' } 	
        },
        {
            name: "404",
            path: "**",
            redirect: "/dashboard",
            meta: { requiresAuth: true }
        }

    ], 
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
}); 

router.beforeResolve((to, from, next) => {   
    if (to.matched.some(record => record.meta.requiresAuth)) {
        //alert( !!localStorage.getItem(config.NAME_PREFIX+'token') )
        let isToken = (!!localStorage.getItem(config.NAME_PREFIX+'token'));
        let isPlan = (!!localStorage.getItem(config.NAME_PREFIX+'plan'));
        if (isToken === false) { 
            window.location.href = config.BASE_URL + 'dashboard/auth';
        } else {    
            if(isPlan === true){
                const plan = JSON.parse(localStorage.getItem(config.NAME_PREFIX+'plan')); 
                if( ( typeof(plan.current_status)!='undefined') && (plan.current_status == true) ) {                     
                    next();
                } else {
                    next();
                }
            } else { 
                next();
            }  
        }  
    }  
    else {
        next();
    }
})

export default router;
