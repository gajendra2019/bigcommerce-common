<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>{{ env('APP_NAME') }}</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <style>


        @media only screen and (max-width:600px) {
            .table_responsive {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch;
            }
            td[class=head-title] {
                font-size: 24px !important;
                padding-bottom: 10px !important;
                line-height: 32px !important;
            }
            td[class=banner] {
                padding: 30px 10px !important;
                font-size: 22px !important;
            }
            td[class=MRRsummary] {
                padding: 30px 0 !important;
            }
            td[class=MRRsummaryFooter] {
                padding: 30px 12px !important;
            }
            
            div[class=mb_View] {
                width: 100% !important;
            }
        }
        @media only screen and (max-width:480px) {
            img[class=logo_img] {
                width: 180px !important;
            }
        }
       
        @media only screen and (max-width:375px) {
            
            span[class=small-title1] {
                font-size: 10px !important;
            }
            span[class=small-title2] {
                font-size: 10px !important;
            }
           span[class=small-title3] {
                font-size: 10px !important;
            }
        }
        @media only screen and (max-width:365px) {
            
            span[class=small-title1] {
                font-size: 9px !important;
            }
            span[class=small-title2] {
                font-size: 9px !important;
            }
           span[class=small-title3] {
                font-size: 9px !important;
            }
        }
    </style>
</head>
<body style="margin:0; padding:0; font-family: 'Roboto', sans-serif !important; color:#484848;" bgcolor="#fcfcfc">
<table align="center" cellpadding="0" cellspacing="0" border="0" style="max-width: 619px;width: 100%;margin: 0 auto">
    <tr>
        <td>
            <div class="mb_View" style="background-color: #fff !important; max-width:619px; margin: 0 auto;">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="logo_block" style="padding-top:30px; padding-bottom:30px; text-align: center;">
                                        <a href="#m_-1778674465433749821_m_-1945850375391101209_m_-6818727870886611857_" target="_blank" target="_blank">
                                            <img src="{{ asset('/app/images/icons/logo.png') }}" class="logo_img" alt="Logo">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
                                <tr>
                                    <td style="padding-bottom:5px; border-radius: 5px 5px 0 0;" bgcolor="#FFFFFF">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center"> 

                                             {{-- Place Your Body Here --}}
                                             @yield('content') 


                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MRRsummaryFooter" style="box-sizing:border-box;display:block;margin:0 auto;max-width:619px;padding:40px 60px" bgcolor="#EFEEEE">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="color:#999999;font-size:14px;line-height:24px;margin:0 auto" align="center">
                                                        Have a question? Email our support team at <br>
                                                        <a href="mailto:{{ env('SUPPORT_EMAIL') }}" style="color:#999999;font-weight:500;text-decoration:none" target="_blank">
                                                            {{ env('SUPPORT_EMAIL') }}
                                                        </a> 
                                                        or visit 
                                                        <a href="https://{{ env('SUPPORT_URL') }}" style="color:#999999;font-weight:500;text-decoration:none" target="_blank">
                                                            {{ env('SUPPORT_URL') }}
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#999999;font-size:14px;line-height:24px;margin:0 auto;padding-top:5px" align="center">You may choose to 
                                                        <a href="javascript:void(0);" style="color:#999999;font-weight:500;text-decoration:none">opt-out</a> 
                                                        if you don't wish to receive this email anymore.
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                          
                        </td>
                    </tr>
                    
                </table>
            </div>
        </tr>
    </td>
</table>
</body>
</html>  


