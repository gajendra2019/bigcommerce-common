@extends('email.layout.layout')

@section('content')
    <tbody>
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td class="banner" style="background: url('{{ asset('/app/images/icons/email_bg.png') }}') no-repeat top center/cover;
                        color: white;
                        display: block;
                        padding: 40px 60px; font-family: 'Roboto', sans-serif !important; text-align:center; font-size:26px;font-weight:500;margin:0">
                            <div class="head-title">
                                {{ env('APP_NAME') }}
                            </div>
                            {{-- <h5 style="color:rgba(255,255,255,.8);font-size:16px;margin:10px 0">
                                Subscription plan activation conformation
                            </h5> --}}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>  
        <tr>
            <td align="center" style="padding-left:20px;padding-right:20px;padding-top:20px;padding-bottom:20px;border-radius:5px 5px 0 0" bgcolor="#f4f4f4">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="padding-top:1px;padding-bottom:15px;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:15px;line-height:24px;font-weight:600;text-transform:capitalize">
                                Hello <?php echo isset($data['receiver']['name'])?(ucwords($data['receiver']['name'])):' User';?>,
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" style="font-family: 'Roboto', sans-serif !important; background-color: #ffffff;
                                margin-bottom: 15px;
                                border-radius: 5px;
                                box-shadow: 0 2px 6px 0 rgba(0,0,0,0.15);">
                                        <tbody> 
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                    Thank you for activating the recurring charge for our <b>{{ env('APP_NAME') }}</b> app. 
                                                </td> 
                                            </tr> 

                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                     <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td class="MRRsummary" style="box-sizing:border-box;display:block;margin:0 auto;max-width:619px;padding:8px 0px 27px;">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="color:#414141;font-size:14px;font-weight:500">
                                                                        <tbody>
                                                                            <tr> 
                                                                                <td bgcolor="#3e3e3e" style="color:#fff;padding: 10px;" 
                                                                                align="center" style="padding: 7px 7px;" colspan="3">
                                                                                    Subscription Detail 
                                                                                </td> 
                                                                            </tr>
                                                                            <tr> 
                                                                                <td bgcolor="#f7f7f7" align="center" style="padding: 7px 7px;">
                                                                                    Plan Name
                                                                                </td> 
                                                                                <td bgcolor="#f7f7f7" align="center" style="padding: 7px 7px;">
                                                                                    Plan Validty
                                                                                </td> 
                                                                                <td bgcolor="#f7f7f7" align="center" style="padding: 7px 7px;">
                                                                                    Next Charge
                                                                                </td>
                                                                            </tr>                                                                       
                                                                            <tr> 
                                                                                <td style="padding: 12px 7px; border-top-color:#e5e5e5;border-top-style:solid;border-top-width:2px;color:#2ca760;font-size:15px" bgcolor="#f7f7f7" align="center">
                                                                                    {{ ( isset($data['store']['planInfo']) && ($data['store']['planInfo']))?$data['store']['planInfo']['plan_name']:'Free Trail' }}
                                                                                </td> 
                                                                                <td style="padding: 12px 7px; border-top-color:#e5e5e5;border-top-style:solid;border-top-width:2px;color:#2ca760;font-size:15px" bgcolor="#f7f7f7" align="center">
                                                                                    {{ ( isset($data['store']['planInfo']) && ($data['store']['planInfo']))?$data['store']['planInfo']['plan_duration']:'7' }} Days
                                                                                </td> 
                                                                                <td style="padding: 12px 7px; border-top-color:#e5e5e5;border-top-style:solid;border-top-width:2px;color:#2ca760;font-size:15px" bgcolor="#f7f7f7" align="center">
                                                                                    {{ ( isset($data['store']['end_date']) && ($data['store']['end_date']))?date('d,M Y h:i:s',strtotime($data['store']['end_date'])):'--' }}
                                                                                </td>
                                                                            </tr>
                                                                        
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr> 
                                                        </tbody>
                                                    </table>                       
                                                </td> 
                                            </tr> 
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px;">
                                                    We appreciate your choice and assure you of the best services, technical support, and any help that you need to attract your customers.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px;font-weight:700;font-size: 13px;">
                                                    Our primary goal is to help store owners to grow their business and for this, we are here with our App, and provide 
                                                    10 hours of free technical support to their store if they need it.
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                    We appreciate your business and look forward to providing you with quality products and excellent customer service.
                                                </td>
                                            </tr> 
                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        <tr>
                            <td style="padding-top:0;padding-bottom:0;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px;font-weight:600">
                                Best Regards,
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:0;padding-bottom:0;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px">
                                {{ env('SUPPORT_REGARDS') }}
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:0;padding-bottom:0;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px">
                                Email : {{ env('SUPPORT_EMAIL') }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody> 
@endsection
