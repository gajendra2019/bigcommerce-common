@extends('email.layout.layout')

@section('content')
    <tbody>
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td class="banner" style="background: url('{{ asset('/app/images/icons/email_bg.png') }}') no-repeat top center/cover;
                        color: white;
                        display: block;
                        padding: 40px 60px; font-family: 'Roboto', sans-serif !important; text-align:center; font-size:26px;font-weight:500;margin:0">
                            <div class="head-title">
                                {{ env('APP_NAME') }}
                            </div> 
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>  
        <tr>
            <td align="center" style="padding-left:20px;padding-right:20px;padding-top:20px;padding-bottom:20px;border-radius:5px 5px 0 0" bgcolor="#f4f4f4">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="padding-top:1px;padding-bottom:15px;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:15px;line-height:24px;font-weight:600;text-transform:capitalize">
                                Hello Support,
                            </td>                            
                        </tr>
                        <tr> 
                             <table class="_app_info_table" style="border:1px solid #ccc;padding:10px;width: 100%; background-color: #fff;">
                                 <thead>
                                     <th style="padding-bottom: 10px" colspan="2">App Information</th>
                                 </thead>
                                 <tbody>
                                     <tr>
                                         <td style="border:1px solid rgb(237,237,237);text-align:left;padding:10px;font-size: 14px;">Store Name</td>
                                         <td style="border:1px solid rgb(237,237,237);text-align:left;padding:10px;font-size: 14px;">
                                            <?php echo isset($data['store']['store_name'])?$data['store']['store_name']:'Store Name';?>
                                        </td>
                                     </tr>
                                     <tr>
                                        <td style="border:1px solid rgb(237,237,237);text-align:left;padding:10px;font-size: 14px;">Store Email</td>
                                        <td style="border:1px solid rgb(237,237,237);text-align:left;padding:10px; font-size: 14px;">
                                            <a href="mailto:<?php echo isset($data['store']['store_email'])?$data['store']['store_email']:'Store Email';?>">
                                                <?php echo isset($data['store']['store_email'])?$data['store']['store_email']:'Store Email';?>
                                            </a> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid rgb(237,237,237);text-align:left;padding:10px;font-size: 14px;">Store URL</td>
                                        <td style="border:1px solid rgb(237,237,237);text-align:left;padding:10px;font-size: 14px;">
                                            <a href="<?php echo isset($data['store']['store_url'])?$data['store']['store_url']:'Store URL';?>" target="_blank">
                                                <?php echo isset($data['store']['store_url'])?$data['store']['store_url']:'Store URL';?>
                                            </a>
                                        </td>
                                    </tr>
                                 </tbody>
                             </table>
                        </tr>
                        <tr>
                            <td  style="background-color:#f4f4f4; padding:0px 15px 0px; font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px;font-weight:600">
                                Best Regards,
                            </td>
                        </tr>
                        <tr>
                            <td style=" background-color:#f4f4f4; padding:0px 15px 0px;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px">
                                {{ env('SUPPORT_REGARDS') }}
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color:#f4f4f4; padding:0px 15px 10px;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px">
                                {{ env('SUPPORT_EMAIL') }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody> 
@endsection
