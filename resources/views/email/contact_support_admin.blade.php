@extends('email.layout.layout')

@section('content')
    <tbody>
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td class="banner" style="background: url('{{ asset('/app/images/icons/email_bg.png') }}') no-repeat top center/cover;
                        color: white;
                        display: block;
                        padding: 40px 60px; font-family: 'Roboto', sans-serif !important; text-align:center; font-size:26px;font-weight:500;margin:0">
                            <div class="head-title">
                                <?php echo isset($data['store']['store_name'])?$data['store']['store_name']:'Demo Store';?> Store
                            </div> 
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>  
        <tr>
            <td align="center" style="padding-left:20px;padding-right:20px;padding-top:20px;padding-bottom:20px;border-radius:5px 5px 0 0" bgcolor="#f4f4f4">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="padding-top:1px;padding-bottom:15px;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:15px;line-height:24px;font-weight:600;text-transform:capitalize">
                                Hey Admin,
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" style="font-family: 'Roboto', sans-serif !important; background-color: #ffffff;
                                margin-bottom: 15px;
                                border-radius: 5px;
                                box-shadow: 0 2px 6px 0 rgba(0,0,0,0.15);">
                                        <tbody>
                                            <tr> 
                                                <td style="font-size:13px;padding-top:10px;color:#000000;line-height:24px;padding-bottom:10px;padding-left:10px;padding-right:10px">
                                                    One of merchant shared inquiry through support, please check below message and merchant details.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                    <b>Name : </b> <?php echo isset($data['enqData']['name'])?$data['enqData']['name']:'';?>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                    <b>Store URL : </b> <?php echo isset($data['store']['store_url'])?$data['store']['store_url']:'';?>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                    <b>Email : </b> <?php echo isset($data['enqData']['email'])?$data['enqData']['email']:'';?>
                                                </td> 
                                            </tr> 
                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                    <b>Message : </b> <?php echo isset($data['enqData']['message'])?$data['enqData']['message']:'';?>
                                                </td> 
                                            </tr> 

                                            <tr>
                                                <td style="font-size:13px;color:#000000;line-height:24px;padding-left:10px;padding-right:10px;padding-bottom:10px">
                                                     Please look this feedback and replying to this email.
                                                </td>
                                            </tr> 
                                    </tbody>
                                </table>
                            </td> 
                        </tr>
                        <tr>
                            <td style="padding-top:0;padding-bottom:0;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px;font-weight:600">
                                Best Regrads,
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:0;padding-bottom:0;font-family: 'Roboto', sans-serif !important;color:#000000;font-size:14px;line-height:24px">
                                {{ env('SUPPORT_REGARDS') }}
                            </td>
                        </tr> 
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody> 
@endsection
