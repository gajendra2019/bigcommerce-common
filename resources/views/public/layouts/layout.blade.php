<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ env('APP_NAME') }}</title>
        <link rel="icon" href="{{ asset('/app/images/icons/fav.png') }}" type="image/png"/> 
        <!-- Fonts -->
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('/app/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/app/css/color-themes.css') }}">
        <link rel="stylesheet" href="{{ asset('/app/css/custom.css') }}"> 
        <link rel="stylesheet" href="{{ asset('/app/css/common_page.css') }}"> 
        <link rel="stylesheet" href="{{ asset('/app/css/public-layout.css') }}">
    </head>
    <body class="app sidebar-mini rtl dark-themes">
        <div id="app">  
            <section class="help_hero">
                <div class="container">
                    <div class="row">
                        <div class="card-wrapper">
                            <div class="brand">
                                <img src="{{ asset('/app/images/icons/logo-white.png') }}" alt="{{ env('APP_NAME') }} Logo" />
                            </div> 
                            <div class="contnet__ele"> 
                                @yield('content')
                            </div>    
                            <div class="outer-footer">
                                @include('public.footer')                         
                            </div>
                        </div>
                    </div>
                </div>
            </section>  
        </div>
        <script type="text/javascript" src="{{ asset('/app/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/app/js/custom.js') }}"></script>
    </body>
</html>