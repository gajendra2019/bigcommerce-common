@extends('public.layouts.layout')

@section('content') 
  <div class="card card__panel faq_box">  
<div class="rp_accord_main_wrapper">
<div class="rp_accord_box_wrapper">
<div class="card-body">
    <h4 class="card-title">Frequently Asked Questions</h4>              
    <ul class="accordion" id="accordion">
        <li class="accordion__item is-open">
            <div class="accordion__link js-accordion-link">Can I disable front-end reward feature?</div>
            <ul class="accordion__submenu js-accordion-submenu" style="display: block;">
                <li class="accordion__submenu-item">
                    Yes, you can disable it. On the dashboard go left menu >> integration, you will see step 1 over there then set off to disable Storefront Reward Pop-up.
                </li>
            </ul>
        </li>
        <li class="accordion__item">
            <div class="accordion__link js-accordion-link">Can I get support from you directly?</div>
            <ul class="accordion__submenu js-accordion-submenu" style="display: none;">
                <li class="accordion__submenu-item">
                    Yes. Just email us at support@hubifyapps.com and our team will help you with any queries like App installation, customisation, and troubleshooting any issues.
                </li>
            </ul>
        </li>
		<li class="accordion__item">
            <div class="accordion__link js-accordion-link">How to get existing customers on the App Dashboard?</div>
            <ul class="accordion__submenu js-accordion-submenu" style="display: none;">
                <li class="accordion__submenu-item">
                    When you install the App then it will export existing customers automatically.
                </li>
            </ul>
        </li>
		<li class="accordion__item">
            <div class="accordion__link js-accordion-link">How much does AI: Loyalty, Referrals & More cost?</div>
            <ul class="accordion__submenu js-accordion-submenu" style="display: none;">
                <li class="accordion__submenu-item">
                    <h2 class="pricePlan"> $ 5.99/Month </h2>
                    <p class="form-control-static plan_basic">Standard Plan</p>
                    <p class="form-control-static"> Unlimited Members &amp; Order</p>
                    <p class="form-control-static"> Take your loyalty program with all core features</p>
                    <p class="form-control-static">&nbsp;7+ Ways to Earn Reward</p>
                    <p class="form-control-static"> Redeem Points &amp; Referral programs</p>
                    <p class="form-control-static"> Full design customization</p>
                    <p class="form-control-static"> Advanced analytics dashboard</p>
                    <p class="form-control-static"> Default rewards emails</p>
                    <p class="form-control-static"> Unlimited custom rewards</p>
                </li>
            </ul>
        </li>
		<li class="accordion__item">
            <div class="accordion__link js-accordion-link">How can I uninstall the app?</div>
            <ul class="accordion__submenu js-accordion-submenu" style="display: none;">
                <li class="accordion__submenu-item">
                    Simply go to your Apps screen in your BigCommerce store and uninstall the App.
                    
                </li>
            </ul>
        </li>
		<li class="accordion__item">
            <div class="accordion__link js-accordion-link">How can I Customize Lanucher pop up ?</div>
            <ul class="accordion__submenu js-accordion-submenu" style="display: none;">
                <li class="accordion__submenu-item">
                <p class="faq_menu">Go to left menu &gt;&gt; Branding &gt;&gt; Launcher</p>
                    there are customization options available to set launcher pop up.
                    <ul>
                        <ol>COLORS</ol>
                        <ol>MOBILE SETTING</ol>
                        <ol>PLACEMENT SETTING</ol>
                        <ol>PROGRAM NAME</ol>
                        <ol>LAUNCHER ICON</ol>
                    </ul>
                    
                </li>
            </ul>
        </li>
		<li class="accordion__item">
            <div class="accordion__link js-accordion-link">How can I adjust points manually for customer?</div>
            <ul class="accordion__submenu js-accordion-submenu" style="display: none;">
                <li class="accordion__submenu-item">
                    <p class="faq_menu">Go to left menu &gt;&gt; customer section &gt;&gt;in the customer list click on  More Info</p>
                    you will see "Adjust balance" button.
                    
                </li>
            </ul>
        </li>
        
    </ul>
</div>
</div>
</div>
</div>
@stop
 
        
 
