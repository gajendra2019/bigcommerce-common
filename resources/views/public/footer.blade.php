<div class="footer_hero">
        <ul class="list-inline ftr__link">
            <li>
                <a class="action_link" href="mailto:support@hubifyapps.com">Email Us</a>
            </li>
            <li>
                <a class="action_link" href="{{ URL::to('/privacy-policy') }}">Privacy Policy</a>
            </li>
            <li>
                <a class="action_link" href="{{ URL::to('/terms-of-services') }}">Terms & Services</a>
            </li>
            <li>
                <a class="action_link" href="{{ URL::to('/faq') }}">FAQ</a> 
            </li> 
        </ul> 
        <p class="not_authftr"> © <?php echo date('Y');?> HubifyApps, All rights reserved</p>
</div>