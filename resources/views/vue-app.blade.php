<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ env('APP_NAME') }}</title>
        <link rel="icon" href="{{ asset('/app/images/icons/fav.png') }}" type="image/png"/> 
        <!-- Fonts -->
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('/app/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/app/css/custom.css') }}">
    </head>
    <body class="app sidebar-mini rtl dark-themes">
        <div id="app"></div>
        <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('/app/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/app/js/bootstrap.min.js') }}"></script>
    </body>
</html>
