<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Plans;

class StorePlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = array([
                'plan_id' => 'free', 
                'product_id' => 'free', 
                'plan_name' => 'Free Plan',  
                'plan_duration' => '7',
                'plan_price' => '0',
                'plan_type' => 'Weekly', 
                'plan_currency' => '$',  
                'plan_description' => '',
                'status' => '0'
            ],
            [
                'plan_id' => 'price_1I93MlBVPMNZdBzlvkeEMKJU', 
                'product_id' => 'prod_IkYTbbix6fDTsw', 
                'plan_name' => 'STANDARD PLAN',  
                'plan_duration' => '30',
                'plan_price' => '7.99',
                'plan_type' => 'Month', 
                'plan_currency' => '$',  
                'plan_description' => '',
                'status' => '1' 
            ]); 

        foreach ($plans as $plan) {
            $model = Plans::where(['plan_id' => $plan['plan_id']])->get()->first();  
            if(!$model) {
                $model = Plans::create($plan);  
            } 
        }  
    }
}
