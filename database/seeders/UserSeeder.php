<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array([
	        'name' 		=> 'Notifications Wiz',
            'email'		=> 'admin@nw-store.com',
	        'password' 	=>  Hash::make('123456'),
            'phone'		=> '1234567890',
            'store_uid'		=> '1',
            'store_id'		=> '123456',
            'status'		=> '1' 
        ]); 

        foreach ($users as $user) {
            $model = User::where(['email' => $user['email']])->get()->first();  
            if(!$model) {
                $model = User::create($user);
            } 
        }  
    }
}
