<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Store;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stores = array(
            [
                'store_id' => '123456', 
                'store_name' => 'Notifications Wiz', 
                'store_email' => 'admin@nw-store.com',  
                'address' => '101,Notifications Wiz Store',
                'country' => 'India',
                'phone' => '1234567890',
                'currency' => 'INR',
                'currency_symbol' => '₹', 
                'start_date' => date('Y-m-d h:i:s'),
                'end_date' => date('Y-m-d h:i:s', strtotime('+2 year')),
                'status' => '1',  
            ] 
        ); 
        foreach ($stores as $store) {
            $model = Store::where(['store_id' => $store['store_id']])->get()->first();  
            if(!$model) {
                $model = Store::create($store);   
            } 
        }  
    }
}
