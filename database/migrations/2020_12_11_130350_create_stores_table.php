<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('stores')) { 
            Schema::create('stores', function (Blueprint $table) {
                $table->id();
                $table->string('store_id')->nullable(); 
                $table->string('store_name')->nullable(); 
                $table->string('store_email')->nullable();
                $table->string('store_hash_key')->nullable(); 
                $table->string('access_token')->nullable();
                $table->string('address')->nullable();
                $table->string('country')->nullable();  
                $table->string('country_code')->nullable(); 
                $table->string('phone')->nullable();
                $table->string('store_url')->nullable();
                $table->string('control_panel_base_url')->nullable();
                $table->string('store_logo_url')->nullable();
                $table->string('order_email')->nullable();
                $table->string('currency')->nullable();
                $table->string('currency_symbol')->nullable();
                $table->string('bc_jsfile')->nullable();
                $table->longText('bc_uuid')->nullable(); 
                $table->string('stripe_client_id')->nullable();
                $table->string('stripe_sub_id')->nullable();
                $table->string('stripe_plan_id')->nullable();            
                $table->dateTime('start_date', 0)->nullable();
                $table->dateTime('end_date', 0)->nullable(); 
                $table->dateTime('cancel_date', 0)->nullable();
                $table->dateTime('deleted_at', 0)->nullable(); 
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
