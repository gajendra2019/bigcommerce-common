<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorePaymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('store_payment_history')) { 
            Schema::create('store_payment_history', function (Blueprint $table) {
                $table->id();
                $table->string('store_id',50)->nullable();
                $table->string('sub_id',50)->nullable(); 
                $table->string('customer',50)->nullable();  
                $table->string('created')->nullable();
                $table->string('current_period_start',50)->nullable();
                $table->string('current_period_end',50)->nullable(); 
                $table->string('collection_method',100)->nullable();  
                $table->string('quantity',50)->nullable();
                $table->string('plan_id',50)->nullable();
                $table->string('plan_active',50)->nullable();
                $table->string('plan_amount',50)->nullable();
                $table->string('plan_currency',50)->nullable();
                $table->string('plan_interval',50)->nullable();
                $table->string('plan_interval_count',50)->nullable();
                $table->string('plan_nickname',50)->nullable();
                $table->string('plan_product',50)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_payment_history');
    }
}
