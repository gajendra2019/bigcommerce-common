<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('app_log')) { 
            Schema::create('app_log', function (Blueprint $table) {
                $table->id();
                $table->string('store_id',50)->nullable();
                $table->string('store_name',50)->nullable(); 
                $table->string('store_email',50)->nullable();  
                $table->string('address')->nullable();
                $table->string('country',50)->nullable();
                $table->string('country_code',20)->nullable(); 
                $table->string('phone',20)->nullable(); 
                $table->timestamp('deleted_at')->nullable()->useCurrent(); 
                $table->enum('status', ['1', '2'])->nullable()->default('1'); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_log');
    }
}
