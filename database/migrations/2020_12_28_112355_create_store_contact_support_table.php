<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreContactSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('store_contact_support')) { 
            Schema::create('store_contact_support', function (Blueprint $table) {
                $table->id();
                $table->string('store_id','50')->nullable(); 
                $table->string('sender_name','50')->nullable(); 
                $table->string('sender_email','100')->nullable(); 
                $table->text('sender_subject')->nullable(); 
                $table->longText('message')->nullable(); 
                $table->string('receiver_email','100')->nullable(); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_contact_support');
    }
}
