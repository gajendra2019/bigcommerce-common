<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreAppStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('store_app_status')) { 
            Schema::create('store_app_status', function (Blueprint $table) {
                $table->id();
                $table->string('store_id',50)->nullable()->default('0');
                $table->enum('is_show', ['0', '1'])->default('1');
                $table->enum('is_setting_updated', ['0', '1'])->default('0'); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_app_status');
    }
}
