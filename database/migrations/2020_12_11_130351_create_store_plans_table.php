<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('store_plans')) { 
            Schema::create('store_plans', function (Blueprint $table) {
                $table->id(); 
                $table->string('plan_id',50)->nullable();
                $table->string('product_id',50)->nullable(); 
                $table->string('plan_name',100)->nullable();  
                $table->bigInteger('plan_duration')->nullable()->default(0);
                $table->float('plan_price')->nullable();
                $table->string('plan_type',50)->nullable(); 
                $table->string('plan_currency',100)->nullable();  
                $table->longText('plan_description')->nullable();
                $table->enum('status', ['0', '1'])->nullable()->default('1');   
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_plans');
    }
}
