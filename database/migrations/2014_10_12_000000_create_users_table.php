<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) { 
            Schema::create('users', function (Blueprint $table) {
                $table->id(); 
                $table->string('name')->nullable();
                $table->string('email')->unique(); 
                $table->string('password'); 
                $table->string('phone')->nullable();
                $table->string('image')->nullable();
                $table->string('address_line')->nullable();
                $table->string('city')->nullable();
                $table->string('state')->nullable();
                $table->string('country')->nullable();
                $table->string('zip')->nullable();
                $table->bigInteger('store_uid')->default(0); 
                $table->string('store_id')->default(0);
                $table->timestamp('email_verified_at')->nullable();
                $table->enum('status', ['0', '1'])->default('1');
                $table->rememberToken();
                $table->timestamps();
            }); 
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
